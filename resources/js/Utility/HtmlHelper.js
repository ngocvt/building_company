"use strict";
var Utility;
(function (Utility) {
    var HtmlHelper = /** @class */ (function () {
        function HtmlHelper() {
        }
        HtmlHelper.getIconLoading = function () {
            return '<span class="fa fa-spinner fa-pulse"></span>';
        };
        HtmlHelper.setFocusToElement = function (element) {
            var s = window.getSelection();
            var r = document.createRange();
            r.setStart(element, element.childElementCount);
            r.setEnd(element, element.childElementCount);
            s.removeAllRanges();
            s.addRange(r);
        };
        HtmlHelper.createAlertWarning = function (message, btn_close) {
            if (btn_close === void 0) { btn_close = true; }
            return this.createAlert(message, 'warning', btn_close);
        };
        HtmlHelper.createAlertSuccess = function (message, btn_close) {
            if (btn_close === void 0) { btn_close = true; }
            return this.createAlert(message, 'success', btn_close);
        };
        HtmlHelper.createAlertInfo = function (message, btn_close) {
            if (btn_close === void 0) { btn_close = true; }
            return this.createAlert(message, 'info', btn_close);
        };
        HtmlHelper.createAlert = function (message, type, btn_close) {
            if (btn_close === void 0) { btn_close = true; }
            var html = '<div class="alert alert-' + type + ' alert-dismissible" role="alert">';
            if (btn_close) {
                html += '  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
            }
            html += message;
            html += '</div>';
            return html;
        };
        return HtmlHelper;
    }());
    Utility.HtmlHelper = HtmlHelper;
})(Utility || (Utility = {}));
