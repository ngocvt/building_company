var Utility;
(function (Utility) {
    Utility.isObject = function (value) {
        return value !== null && typeof value === 'object';
    };

    Utility.isPlainObject = function (value) {
        return Utility.isObject(value) && value.__proto__ === Object.prototype;
    };

    Utility.Http = {};

    Utility.Http.getErrorMessage = function (xhr) {
        let error_message = '';
        let error_trace = '';
        let response;
        if (!Utility.isObject(xhr)) {
            error_message = xhr.toString();
        } else if (xhr instanceof Error) {
            error_message = xhr.toString();
        } else if (xhr.responseJSON && xhr.responseJSON.error) {
            error_message = xhr.responseJSON.error || '';
            error_trace = xhr.responseJSON.trace || '';
        } else if (xhr.responseText) {
            try {
                response = xhr.responseText ? JSON.parse(xhr.responseText) : {};
            } catch (e) {
                response = xhr.responseText;
            }
            if (Utility.isPlainObject(response) && (response.error || response.message)) {
                error_message = response.error  || response.message;
                error_trace = response.trace || '';
            } else {
                error_message = xhr.responseText;
            }
        } else {
            error_message = xhr.status + " " + xhr.statusText;
        }
        console.warn("error_message:", error_message);
        error_trace && console.warn(error_trace);
        return error_message;
    };

    Utility.Http.setToken = function (token) {
        jQuery.ajaxSetup({
            headers: {'X-AJAX-TOKEN': token}
        });
    };

    /** Session expired, redirect to login page */
    Utility.Http.setFailFallback = function () {
        const HTTP_UNAUTHORIZED = 401;
        $(document).ajaxError(function (event, jqxhr, settings, thrownError) {
            if (jqxhr.status === HTTP_UNAUTHORIZED && jqxhr.responseJSON) {
                if (jqxhr.responseJSON.message) {
                    alert(jqxhr.responseJSON.message);
                }

                if (jqxhr.responseJSON.redirect) {
                    location.href = jqxhr.responseJSON.redirect;
                }
            }
        });
    }
})(Utility || (Utility = {}));
