function isValidUploadFile(fileUpload) {
    var valid = true;

    if (fileUpload.size / 1024 > 4000) {
        valid = false;
    }

    return valid;
}

function showErrorMessage(errMessage, position, cssMessage) {
    cssMessage = typeof (cssMessage) != 'undefined' ? cssMessage : 'alert-success';

    var html = '<div class="alert ' + cssMessage + '">\n' +
        '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span></button>\n' +

        '<span class="message">' + errMessage + '</span>\n' +
        '</div>';

    $(position).empty().append(html);
}

function b64toBlob(b64Data, contentType, sliceSize) {
    contentType = contentType || '';
    sliceSize = sliceSize || 512;

    var byteCharacters = atob(b64Data);
    var byteArrays = [];

    for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
        var slice = byteCharacters.slice(offset, offset + sliceSize);

        var byteNumbers = new Array(slice.length);
        for (var i = 0; i < slice.length; i++) {
            byteNumbers[i] = slice.charCodeAt(i);
        }

        var byteArray = new Uint8Array(byteNumbers);

        byteArrays.push(byteArray);
    }

    var blob = new Blob(byteArrays, {type: contentType});
    return blob;
}

function getErrorForm(err, position) {
    var errMessage = '';

    if (typeof (err.errors != 'undefined')) {

        for (var key in err.errors) {
            var itemErrors = err.errors[key];

            for (var index in itemErrors) {
                var errMess = itemErrors[index];

                errMessage += errMess + '<br/> '
            }
        }
    } else {
        errMessage = err.message;
    }

    showErrorMessage(errMessage, position, 'alert-danger');
}
