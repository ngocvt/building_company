@extends('management.layout.app')
@section('body_class', 'present_text')
@section('page_title', isset($presentText->id) ? 'Sửa' : 'Tạo Mới')
@section('heading_title', isset($presentText->id) ? 'Sửa' : 'Tạo Mới')
@section('content')
    <div class="container h-100">
        <div class="row h-100">
            <div class="col-md-12 col-md-offset-2">
                <form class="form-change-pass" method="post" action="{{ route('manager.save.present.text') }}"
                      id="form_change_pass">
                    <input type="hidden" id="present_text_id" name="present_text_id" value="{{ $presentText->id }}">
                    @csrf
                    @include('layout.includes.message')
                    <div class="row main_post mt-5">
                        <div class="col-md-10 post_data mt-3">
                            <div class="row form-group">
                                <div class="col-md-12">
                                    <p style="font-weight: bold">Tên <span style="color: red">*</span></p>
                                    <input required class="form-control" type="text" name="present_text_name"
                                           value="{{ $presentText->present_name }} ">
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-12">
                                    <p style="font-weight: bold">Miêu tả <span style="color: red">*</span></p>
                                    <input required class="form-control" type="text" name="description"
                                           value="{{ $presentText->description }} ">
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-12">
                                    <p style="font-weight: bold">Icon <span style="color: red">*</span></p>
                                    <input required class="form-control" type="text" name="icon"
                                           value="{{ $presentText->icon }} ">
                                </div>
                                <div class="col-md-12">
                                    <a href="https://www.flaticon.com">Tìm hiểu về các icon tại đây</a>
                                    <br>
                                    <span style="font-style: italic">Sau khi tìm được icon yêu thích, copy tên icon và nhập vào ô trên.</span>
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-2" style="border-left: 1px solid #b9c8d0">
                                    <div class="row form-group">
                                        <div class="col-md-12">
                                            <input type="checkbox" name="publish" value="1"
                                                   id="publish" {{ $presentText->publish === 0 ? 'checked' : '' }}>
                                            Publish
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-5">
                        <div class="col text-center">
                            <button class="btn btn-primary"
                                    type="submit">{{ $presentText->id ? 'Lưu Thay Đổi' : 'Tạo Mới' }}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        function readURL(input) {
            if (input.files && input.files[0]) {
                if (isValidUploadFile(input.files[0])) {
                    if (!iEdit.open(input.files[0], true, function (res) {
                        $('.file_upload img').attr('src', res);

                        var ImageURL = res;
                        var block = ImageURL.split(";");
                        var contentType = block[0].split(":")[1];
                        var realData = block[1].split(",")[1];

                        blobImg = b64toBlob(realData, contentType);

                    })) {
                        alert("Please choose another picture");
                    }
                } else {
                    var errMessage = $(input).data('err-message');
                    alert(errMessage);
                }
            }
        }

        $(function () {
            var container = $('.file_upload'), inputFile = $('#image_present'), img, btn, txt = 'Chọn Ảnh',
                txtAfter = 'Chọn Ảnh Khác';

            if (!container.find('#upload').length) {
                container.find('.input').append('<input type="button" value="' + txt + '" id="upload">');
                btn = $('#upload');
                container.prepend('<img src="" class="hidden" alt="Uploaded file" id="uploadImg" width="100">');
                img = $('#uploadImg');
            }

            btn.on('click', function () {
                img.animate({opacity: 0}, 300);
                $('#exist_image').remove();
                inputFile.click();
            });

            inputFile.on('change', function (e) {
                var i = 0;
                for (i; i < e.originalEvent.srcElement.files.length; i++) {
                    var file = e.originalEvent.srcElement.files[i],
                        reader = new FileReader();

                    reader.onloadend = function () {
                        img.attr('src', reader.result).animate({opacity: 1}, 700);
                    };
                    reader.readAsDataURL(file);
                    img.removeClass('hidden');
                }

                btn.val(txtAfter);
            });
        });
    </script>
    <style>
        .main_post {
            background: #DADADA;
            border: 1px solid #b9c8d0;
        }

        .file_upload label {
            display: block;
            margin: 0 auto 10px;
            word-wrap: break-word;
            color: #B9C8D0;
        }

        .hidden, #uploadImg:not(.hidden) + .file_upload label {
            display: none;
        }

        #image_present {
            display: none;
        }

        #upload {
            display: block;
            padding: 10px 25px;
            border: 0;
            font-size: 15px;
            letter-spacing: 0.05em;
            cursor: pointer;
            background: #B9C8D0;
            outline: none;
            transition: .3s ease-in-out;
        }

        #uploadImg #exist_image {
            margin: 15px 0;
        }
    </style>
@endsection
