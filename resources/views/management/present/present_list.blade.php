<div class="row">
    <div class="col-md-12">
        <h1>Text</h1>
        <div class="table-responsive">
            <table class="table table-striped">
                <thead class="thead-dark">
                <tr>
                    <th>#</th>
                    <th>Tên</th>
                    <th>Mô Tả</th>
                    <th class="text-center">Publish</th>
                    <th>Sửa</th>
                    <th>Xóa</th>
                </tr>
                </thead>
                <tbody>
                @foreach($presentTexts as $presentText)
                    <tr class="">
                        <td>{{ $presentText->id }}</td>
                        <td>
                            <a class="{{ $presentText->publish === 1 ? 'text-secondary' : 'text-success' }}"
                               href="">
                                {{ $presentText->present_name }}
                            </a>
                        </td>
                        <td>{{ $presentText->description }}</td>
                        <td class="text-center {{ $presentText->publish === 1 ? 'text-danger' : 'text-success' }}">
                            <i class="fa {{ $presentText->publish === 0 ? 'fa-check' : 'fa-ban' }}"></i>
                        </td>
                        <td>
                            <a type="button" class="btn btn-primary post-edit-link"
                               href="{{ route('manager.present.edit', ['present_text_id' => $presentText->id]) }}">
                                <i class="fa fa-edit"></i>
                            </a>
                        </td>
                        <td>
                            <a type="button" class="btn btn-danger post-edit-link" href="">
                                <i class="fa fa-trash"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="col-md-12">
        <h1>Image</h1>
        <div class="table-responsive">
            <table class="table thead-dark">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Tên</th>
                    <th>Ảnh</th>
                    <th class="text-center">Publish</th>
                    <th>Sửa</th>
                    <th>Xóa</th>
                </tr>
                </thead>
                <tbody>
                @foreach($presentImages as $presentImage)
                    <tr class="">
                        <td>{{ $presentImage->id }}</td>
                        <td>
                            <a class="{{ $presentImage->publish === 1 ? 'text-secondary' : 'text-success' }}"
                               href="">
                                {{ $presentImage->image_name }}
                            </a>
                        </td>
                        <td>
                            <img src="{{ getPathImage($presentImage->image_url) }}" alt="" width="100" height="50">
                        </td>
                        <td class="text-center {{ $presentImage->publish === 1 ? 'text-danger' : 'text-success' }}">
                            <i class="fa {{ $presentImage->publish === 0 ? 'fa-check' : 'fa-ban' }}"></i>
                        </td>
                        <td>
                            <a type="button" class="btn btn-primary post-edit-link"
                               href="{{ route('manager.present.edit', ['present_image_id' => $presentImage->id]) }}">
                                <i class="fa fa-edit"></i>
                            </a>
                        </td>
                        <td>
                            <a type="button" class="btn btn-danger post-edit-link" href="">
                                <i class="fa fa-trash"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
