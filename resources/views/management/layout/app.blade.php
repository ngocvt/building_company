<!doctype html>
<html lang="vie">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('page_title')</title>
    <script src="{{ mix('js/base.js') }}" type="text/javascript"></script>
    <script src="{{ url("/js/bluebird.min.js") }}" type="text/javascript"></script>
    <script src="{{ mix("/js/vendors.js") }}"></script>
    <link rel="icon" href="{{ url('/icon.jpg') }}" type="image/x-icon">
    <link href="{{ mix('css/management.css') }}" rel="stylesheet" type="text/css">
</head>
<body @yield('body_attribute') class="page_body {{ @$_COOKIE['toggle_menu_status_close'] == '1' ? 'menu_close' : '' }} @yield('body_class')">
<div class="wrapper_page">
    <div class="wrapper">
        @if (isManagement())
            <div class="left_column">
                @include('management.partials.setting_menu')
            </div>
        @endif
        <div class="right_column">
            @if (isManagement())
                @include('management.partials.menu_top')
            @endif
            <main class="main_page">
                @yield('content')
            </main>
            <div class="app_version">version: 1.0</div>
        </div>
    </div>
</div>
</body>
</html>
