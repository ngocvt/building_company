<div class="table-responsive">
    <table class="table highlight_row">
        <thead>
        <tr>
            <th>#</th>
            <th>Tên Chuyên Mục</th>
            <th>Ảnh Chuyên Mục</th>
            <th class="text-center">Publish</th>
            <th>Sửa</th>
            <th>Xóa</th>
        </tr>
        </thead>
        <tbody>
        @foreach($categories as $category)
            <tr class="">
                <td>{{ $category->id }}</td>
                <td>
                    <a class="{{ $category->publish === 1 ? 'text-secondary' : 'text-success' }}"
                       href="{{ route('manager.category.detail', ['category_id' => $category->id]) }}">
                        {{ $category->category_name }}
                    </a>
                </td>
                <td>
                    <img src="{{ getPathImage($category->background_url) }}" alt="" width="100" height="50">
                </td>
                <td class="text-center {{ $category->publish === 1 ? 'text-danger' : 'text-success' }}">
                    <i class="fa {{ $category->publish === 0 ? 'fa-check' : 'fa-ban' }}"></i>
                </td>
                <td>
                    <a type="button" class="btn btn-primary post-edit-link"
                       href="{{ route('manager.category.edit', ['category_id' => $category->id]) }}">
                        <i class="fa fa-edit"></i>
                    </a>
                </td>
                <td>
                    <a type="button" class="btn btn-danger post-edit-link" href="">
                        <i class="fa fa-trash"></i>
                    </a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>

<nav aria-label="Page navigation" class="management-pagination mt-5">
    {!! $paging !!}
</nav>
