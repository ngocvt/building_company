<div class="table-responsive">
    <table class="table highlight_row">
        <thead>
        <tr>
            <th>#</th>
            <th>Tên Bài Viết</th>
            <th>Miêu Tả</th>
            <th>Nội Dung</th>
            <th>Chuyên Mục</th>
            <th class="text-center">Sửa</th>
            <th class="text-center">Xóa</th>
        </tr>
        </thead>
        <tbody>
        @foreach($postList as $post)
            <tr class="">
                <td>{{ $post->post_id }}</td>
                <td>{{ $post->post_name }}</td>
                <td>{{ $post->description }}</td>
                <td>{{ getCategoryById($post->category_id)->category_name }}</td>
                <td class="text-center">
                    <a type="button" class="btn btn-primary post-edit-link"
                       href="{{ route('manager.post.edit', ['post_id' => $post->post_id]) }}">
                        <i class="fa fa-edit"></i>
                    </a>
                </td>
                <td class="text-center">
                    {!! Form::open([
                         'method' => 'POST',
                         'route' => ['manager.post.delete', ['post_id' => $post->post_id]],
                         'onsubmit' => "return confirm('Are you sure you want to delete?')"
                     ])
                 !!}
                    {!! Form::button('<i class="fa fa-trash"></i>', ['class' => 'btn btn-danger', 'type' => 'submit']) !!}
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>

<nav aria-label="Page navigation" class="management-pagination mt-5">
    {!! $paging !!}
</nav>
