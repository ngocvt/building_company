@extends('management.layout.app')

@section('body_class', 'page_all_categories')
@section('page_title', 'Quản Lý Chuyên Mục')
@section('heading_title', 'Quản Lý Chuyên Mục')

@section('content')
    <div class="filter page_data">
        <input type="hidden" name="app_route_list" value="{{ route('manager.post.categories') }}">
        @csrf
        <div class="row">
            <div class="col-md-9">
                <div class="row">
                    <label class="col-sm-2 col-lg-2 col-form-label">Tên Chuyên Mục</label>
                    <div class="col-sm-6 col-lg-6">
                        <input type="text" name="category_name" class="category_name form-control form-control-lg">
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="row">
                    <div class="col-sm-4">
                        <button class="app-btn app-btn-blue-darker btn-search btn-responsive mb-5 btn-search-category"
                                type="button"><i class="fa fa-search mr-2"></i>Search
                        </button>
                    </div>
                    <div class="col-sm-6">
                        <a id="btn-register-new-item" class="app-btn btn-success btn-responsive"
                           href="{{ route('manager.create.category') }}" style="color: #ffffff; float: right;">New
                            Category</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="page_data">@include('layout.includes.message')</div>

    <div class="page_data data_table" data-title-loading="Loading..." data-message-success="labels.Delete item success">

    </div>
    <script>
        function DataTable() {
            this.url = $("[name=app_route_list]").val();
            this.$view_table = $(".data_table");
        }

        DataTable.prototype.loadData = function(page_number) {
            this.$view_table.text(this.$view_table.attr('data-title-loading'));
            page_number = page_number || 1;

            $.post(this.url, {
                '_token': $(".filter [name=_token]").val(),
                'category_name':  $(".filter [name=category_name]").val(),
                'page': page_number,
            }).done(function (html) {
                this.$view_table.html(html);
            }.bind(this)).fail(function (e) {
                this.$view_table.text(e);
                console.log(e);
            }.bind(this));
        };

        DataTable.prototype.initEvent = function() {
            // Click on search button
            $(".btn-search-category").off("click").on('click', function (e) {
                this.loadData();
                e.preventDefault();
                return false;
            }.bind(this));

            // When click on paging
            this.$view_table.on("click.management-pagination", ".management-pagination .page-link", function (e) {
                var page_number = $(e.currentTarget).attr('href').substr(1);
                if (page_number) {
                    this.loadData(page_number);
                }

                e.preventDefault();
                return false;
            }.bind(this));
        };

        $(document).ready(function () {
            var oDataTable = new DataTable();
            oDataTable.initEvent();
            oDataTable.loadData();
        }) ;
    </script>
@endsection
