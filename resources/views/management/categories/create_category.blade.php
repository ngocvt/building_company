@extends('management.layout.app')
@section('body_class', 'category_management')
@section('page_title', isset($category->id) ? 'Sửa Chuyên Mục' : 'Tạo Chuyên Mục Mới')
@section('heading_title', isset($category->id) ? 'Sửa Chuyên Mục' : 'Tạo Chuyên Mục Mới')
@section('content')
    <div class="container h-100">
        <input type="hidden" name="upload_image" value="{{ route('upload-image') }}">
        <div class="row h-100">
            <div class="col-md-12 col-md-offset-2">
                <form class="form-change-pass" method="post" action="{{ route('manager.save.category') }}"
                      id="form_change_pass" enctype="multipart/form-data">
                    <input type="hidden" id="category_id" name="category_id" value="{{ $category->id }}">
                    @csrf
                    @include('layout.includes.message')
                    <div class="row main_post mt-5">
                        <div class="col-md-10 post_data mt-3">
                            <div class="row form-group">
                                <div class="col-md-12">
                                    <p style="font-weight: bold">Tên Chuyên Mục <span style="color: red">*</span></p>
                                    <input required class="form-control" type="text" name="category_name"
                                           value="{{ $category->category_name }} ">
                                </div>
                            </div>

                            <div class="row form-group">
                                <label class="label" for="" style="font-weight: bold; margin: 0 15px">
                                    Avatar Chuyên Mục
                                    <span style="color: red">
                                            *
                                        </span>
                                </label>
                                <div class="col-md-12 file_upload mb-2">
                                    @if($category->background_url)
                                        <img src="{{ asset($category->background_url) }}" width="100" alt="" id="exist_image"
                                             style="opacity: 1">
                                    @endif
                                    <div class="input">
                                        <input name="image_category" id="image_category" type="file" accept="image/*">
                                    </div>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-md-2" style="border-left: 1px solid #b9c8d0">
                                    <div class="row form-group">
                                        <div class="col-md-12">
                                            <input type="checkbox" name="publish_category" value="1" id="publish_category" {{ $category->publish === 0 ? 'checked' : '' }}>
                                            Publish
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-5">
                        <div class="col text-center">
                            <button class="btn btn-primary"
                                    type="submit">{{ $category->id ? 'Lưu Thay Đổi' : 'Tạo Mới' }}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        function readURL(input) {
            if (input.files && input.files[0]) {
                if (isValidUploadFile(input.files[0])) {
                    if (!iEdit.open(input.files[0], true, function (res) {
                        $('.file_upload img').attr('src', res);

                        var ImageURL = res;
                        var block = ImageURL.split(";");
                        var contentType = block[0].split(":")[1];
                        var realData = block[1].split(",")[1];

                        blobImg = b64toBlob(realData, contentType);

                    })) {
                        alert("Please choose another picture");
                    }
                }
                else {
                    var errMessage = $(input).data('err-message');
                    alert(errMessage);
                }
            }
        }

        $(function () {
            var container = $('.file_upload'), inputFile = $('#image_category'), img, btn, txt = 'Chọn Ảnh',
                txtAfter = 'Chọn Ảnh Khác';

            if (!container.find('#upload').length) {
                container.find('.input').append('<input type="button" value="' + txt + '" id="upload">');
                btn = $('#upload');
                container.prepend('<img src="" class="hidden" alt="Uploaded file" id="uploadImg" width="100">');
                img = $('#uploadImg');
            }

            btn.on('click', function () {
                img.animate({opacity: 0}, 300);
                $('#exist_image').remove();
                inputFile.click();
            });

            inputFile.on('change', function (e) {
                var i = 0;
                for (i; i < e.originalEvent.srcElement.files.length; i++) {
                    var file = e.originalEvent.srcElement.files[i],
                        reader = new FileReader();

                    reader.onloadend = function () {
                        img.attr('src', reader.result).animate({opacity: 1}, 700);
                    };
                    reader.readAsDataURL(file);
                    img.removeClass('hidden');
                }

                btn.val(txtAfter);
            });
        });
    </script>
    <style>
        .main_post {
            background: #DADADA;
            border: 1px solid #b9c8d0;
        }

        .file_upload label {
            display: block;
            margin: 0 auto 10px;
            word-wrap: break-word;
            color: #B9C8D0;
        }

        .hidden, #uploadImg:not(.hidden) + .file_upload label {
            display: none;
        }

        #image_category {
            display: none;
        }

        #upload {
            display: block;
            padding: 10px 25px;
            border: 0;
            font-size: 15px;
            letter-spacing: 0.05em;
            cursor: pointer;
            background: #B9C8D0;
            outline: none;
            transition: .3s ease-in-out;
        }

        #uploadImg #exist_image{
            margin: 15px 0;
        }
    </style>
@endsection
