@extends('management.layout.app')

@section('body_class', 'page_all_post')
@section('page_title', 'Các Bài Viết')
@section('heading_title', 'Các Bài Viết')

@section('content')
    <div class="filter page_data">
        <input type="hidden" name="app_route_list" value="{{ route('manager.post.list') }}">
        <input type="hidden" name="update_post_publish" value="{{ route('manager.publish.post') }}">
        @csrf
        <div class="row">
            <div class="col-sm-12 col-md-8">
                <div class="form-group row">
                    <label class="col-sm-2 col-lg-2 col-form-label">Tên Bài</label>
                    <div class="col-sm-10 col-lg-10">
                        <input type="text" name="post_name" class="post_name form-control form-control-lg">
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 col-md-8">
                <div class="form-group row input-group-lg">
                    <label class="col-sm-2 col-lg-2 col-form-label">Ngày Tạo</label>
                    <div class="col-sm-4 col-lg-4">
                        <input type="text" name="hire_date_from" id="hire_date_from"
                               class="date-from form-control form-control-lg date-picker">
                    </div>
                    <label class="col-sm-1 col-lg-1 col-form-label text-center">-</label>
                    <div class="col-sm-5 col-lg-5">
                        <input type="text" name="hire_date_to" id="hire_date_to"
                               class="date-to form-control form-control-lg date-picker">
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-4">
                <div class="form-group row">
                    <div class="col-sm-5 text-right">
                        <button class="btn btn-primary btn-search-post" type="button"><i class="fa fa-search mr-2"></i>Search
                        </button>
                    </div>
                    <div class="col-sm-6">
                        <a class="btn btn-success" href="{{ route('manager.create.post') }}"
                           style="color: #ffffff; float: right;">New Post</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="page_data">@include('layout.includes.message')</div>

    <div class="page_data data_table" data-title-loading="Loading..." data-message-success="labels.Delete item success">

    </div>
    <script>
        function DataTable() {
            this.url = $("[name=app_route_list]").val();
            this.$view_table = $(".data_table");
        }

        DataTable.prototype.loadData = function (page_number) {
            this.$view_table.text(this.$view_table.attr('data-title-loading'));
            page_number = page_number || 1;

            $.post(this.url, {
                '_token': $(".filter [name=_token]").val(),
                'page': page_number,
                'post_name': $(".filter .post_name").val(),
                'date_from': $("#hire_date_from").val(),
                'date_to': $("#hire_date_to").val(),
            }).done(function (html) {
                this.$view_table.html(html);
            }.bind(this)).fail(function (e) {
                this.$view_table.text(e);
                console.log(e);
            }.bind(this));
        };

        DataTable.prototype.initEvent = function () {
            $('.date-from').datetimepicker({
                useCurrent: false,
                format: 'YYYY/MM/DD'
            });

            $('.date-to').datetimepicker({
                format: 'YYYY/MM/DD'
            });

            $(".date_from").on("dp.change", function (e) {
                $('.date-from').data("DateTimePicker").minDate(e.date);
            });
            $(".date_to").on("dp.change", function (e) {
                $('.date-to').data("DateTimePicker").maxDate(e.date);
            });

            // Click on search button
            $(".btn-search-post").off("click").on('click', function (e) {
                this.loadData();
                e.preventDefault();
                return false;
            }.bind(this));

            // When click on paging
            this.$view_table.on("click.management-pagination", ".management-pagination .page-link", function (e) {
                var page_number = $(e.currentTarget).attr('href').substr(1);
                if (page_number) {
                    this.loadData(page_number);
                }

                e.preventDefault();
                return false;
            }.bind(this));

            this.$view_table.off("click.publish_post").on("click.publish_post", '.publish_post', function (e) {
                editPublish($(e.currentTarget));
                e.preventDefault();
                return false;
            }.bind(this));

        };

        function editPublish($element) {
            if ($element.hasClass('editing')) {
                return;
            }
            let post_id = $element.addClass('editing spinner-border').data('post_id');
            let publish = $element.find('input').val();
            let url = $("[name=update_post_publish]").val();
            $.post(url, {
                '_token': $(".filter [name=_token]").val(),
                'post_id': post_id,
                'publish_post': publish == 1 ? 0 : 1,
            }).done(function (results) {
                $element.removeClass('editing spinner-border');
                if (results.publish == 0) {
                    $element.removeClass('text-danger').addClass('text-success');
                    $element.find('i').removeClass('fa-ban').addClass('fa-check');
                    $element.closest('tr').find('td:eq(1)').removeClass('text-secondary').addClass('text-success');
                } else {
                    $element.removeClass('text-success').addClass('text-danger');
                    $element.find('i').removeClass('fa-check').addClass('fa-ban');
                    $element.closest('tr').find('td:eq(1)').removeClass('text-success').addClass('text-secondary');
                }
                $element.find('input').val(results.publish);

            }).fail(function (reason) {
                alert('publish bài viết bị lỗi');
                $element.removeClass('editing spinner-border');
            });
        }

        $(document).ready(function () {
            var oDataTable = new DataTable();
            oDataTable.initEvent();
            oDataTable.loadData();
        });
    </script>
    <style>
        .publish_post {
            cursor: pointer;
        }
    </style>
@endsection
