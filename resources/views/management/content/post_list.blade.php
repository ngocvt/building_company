<div class="table-responsive">
    <table class="table highlight_row">
        <thead>
        <tr>
            <th>#</th>
            <th>Tên Bài Viết</th>
            <th class="text-center">Lượt Xem</th>
            <th>Chuyên Mục</th>
            <th class="text-center">Publish</th>
            <th>Ngày Tạo</th>
            <th class="text-center">Sửa</th>
            <th class="text-center">Xóa</th>
        </tr>
        </thead>
        <tbody>
        @foreach($postList as $post)
            <tr class="1">
                <td>{{ $post->post_id }}</td>
                <td class="{{ $post->publish === 1 ? 'text-secondary' : 'text-success' }}">{{ $post->post_name }}</td>
                <td class="text-center">{{ $post->views }}</td>
                <td>{{ getCategoryById($post->category_id)->category_name }}</td>
                <td class="text-center">
                    <span class="publish_post {{ $post->publish === 1 ? 'text-danger' : 'text-success' }}"
                          data-post_id="{{$post->post_id}}">
                        <i id="post_publish_{{$post->post_id}}"
                           class="fa {{ $post->publish === 0 ? 'fa-check' : 'fa-ban' }}"></i>
                        <input type="hidden" name="publish_value" value="{{ $post->publish  }}">
                    </span>
                </td>
                <td>{{ $post->created_at->format('d-m-yy') }}</td>
                <td class="text-center">
                    <a type="button" class="btn btn-primary post-edit-link"
                       href="{{ route('manager.post.edit', ['post_id' => $post->post_id]) }}">
                        <i class="fa fa-edit"></i>
                    </a>
                </td>
                <td class="text-center">
                    {!! Form::open([
                         'method' => 'POST',
                         'route' => ['manager.post.delete', ['post_id' => $post->post_id]],
                         'onsubmit' => "return confirm('Bạn muốn xóa bài viết này?')"
                     ])
                 !!}
                    {!! Form::button('<i class="fa fa-trash"></i>', ['class' => 'btn btn-danger', 'type' => 'submit']) !!}
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>

<nav aria-label="Page navigation" class="management-pagination mt-5">
    {!! $paging !!}
</nav>
