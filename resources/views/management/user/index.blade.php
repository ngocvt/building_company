@extends('management.layout.app')
@section('body_class', 'page_user')
@section('page_title', 'Tài Khoản')
@section('heading_title', 'Tài Khoản')
@section('content')
    <form class="form-user-profile" method="post" action="{{ route('manager.save.user') }}"
          enctype="multipart/form-data">
        <input type="hidden" name="user_id" value="{{ getCurrentUser()->id }}">
        @csrf
        @include('layout.includes.message')
        <div class="page_data">
            <div class="form-group">
                <label for="user_name" class="label-normal">Tên Đăng Nhập</label>
                <input type="text" class="form-control" id="name" name="name" placeholder=""
                       value="{{ getCurrentUser()->name }}">
            </div>
            <div class="form-group">
                <label for="user_name" class="label-normal">Email</label>
                <input type="text" class="form-control" id="email" name="email" placeholder=""
                       value="{{ getCurrentUser()->email }}">
            </div>
{{--            <div class="form-group">--}}
{{--                <label for="password" class="label-normal">Mật Khẩu Cũ </label>--}}
{{--                <input type="password" class="form-control" id="old_password" name="old_password" placeholder=""--}}
{{--                       value="">--}}
{{--            </div>--}}
{{--            <div class="form-group">--}}
{{--                <label for="password" class="label-normal">Mật Khẩu Mới</label>--}}
{{--                <input type="password" class="form-control" id="password" name="password" placeholder="" value="">--}}
{{--            </div>--}}
{{--            <div class="form-group">--}}
{{--                <label for="password" class="label-normal">Nhập Lại Mật Khẩu</label>--}}
{{--                <input type="password" class="form-control" id="password_confirmation" name="password_confirmation"--}}
{{--                       placeholder="">--}}
{{--            </div>--}}
            <div class="app-btn-group">
                <div class="form-group">
                    <label for="">Ảnh</label>
                    <div class="col-md-12 file_upload mt-5">
                        @if(getCurrentUser()->avatar)
                            <img src="{{ getPathImage(getCurrentUser()->avatar) }}" alt="" id="exist_image" width="200px"
                                 height="200px">
                        @endif
                        <div class="input">
                            <input name="user_image" id="user_image" type="file" accept="image/*">
                        </div>
                    </div>
                </div>
                <button class="btn btn-primary" type="submit">Thay Đổi</button>
            </div>
        </div>
    </form>
    <script type="text/javascript">
        function readURL(input) {
            if (input.files && input.files[0]) {
                if (isValidUploadFile(input.files[0])) {
                    if (!iEdit.open(input.files[0], true, function (res) {
                        $('.file_upload img').attr('src', res);

                        var ImageURL = res;
                        var block = ImageURL.split(";");
                        var contentType = block[0].split(":")[1];
                        var realData = block[1].split(",")[1];

                        blobImg = b64toBlob(realData, contentType);

                    })) {
                        alert("Please choose another picture");
                    }
                } else {
                    var errMessage = $(input).data('err-message');
                    alert(errMessage);
                }
            }
        }

        $(function () {
            var container = $('.file_upload'), inputFile = $('#user_image'), img, btn, txt = 'Chọn Ảnh',
                txtAfter = 'Chọn Ảnh Khác';

            if (!container.find('#upload').length) {
                container.find('.input').append('<input type="button" value="' + txt + '" id="upload">');
                btn = $('#upload');
                container.prepend('<img src="" class="hidden" alt="Uploaded file" id="uploadImg" width="100">');
                img = $('#uploadImg');
            }

            btn.on('click', function () {
                img.animate({opacity: 0}, 300);
                $('#exist_image').remove();
                inputFile.click();
            });

            inputFile.on('change', function (e) {
                var i = 0;
                for (i; i < e.originalEvent.srcElement.files.length; i++) {
                    var file = e.originalEvent.srcElement.files[i],
                        reader = new FileReader();

                    reader.onloadend = function () {
                        img.attr('src', reader.result).animate({opacity: 1}, 700);
                    };
                    reader.readAsDataURL(file);
                    img.removeClass('hidden');
                }

                btn.val(txtAfter);
            });
        });
    </script>
    <style>
        .main_post {
            background: #DADADA;
            border: 1px solid #b9c8d0;
        }

        .file_upload label {
            display: block;
            margin: 0 auto 10px;
            word-wrap: break-word;
            color: #B9C8D0;
        }

        .hidden, #uploadImg:not(.hidden) + .file_upload label {
            display: none;
        }

        #user_image {
            display: none;
        }

        #upload {
            display: block;
            padding: 10px 25px;
            border: 0;
            font-size: 15px;
            letter-spacing: 0.05em;
            cursor: pointer;
            background: #B9C8D0;
            outline: none;
            transition: .3s ease-in-out;
        }

        #uploadImg #exist_image {
            margin: 15px 0;
        }
    </style>
@endsection
