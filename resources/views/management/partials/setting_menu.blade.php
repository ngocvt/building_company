<?php
$current_route = \Route::currentRouteName();
$fn_active = function ($routes = []) use ($current_route) {
    $routes = is_string($routes) ? [$routes] : $routes;
    foreach ($routes as $route) {
        if ($route == $current_route) {
            return true;
        }
    }

    return false;
};

$fn_is_active = function ($menus) {
    foreach ($menus['items'] as $menu) {
        if ($menu['active']) {
            return true;
        }
    }

    return false;
};

$menus = [
    [
        'icon' => 'fa fa-tasks',
        'title' => ('Tất Cả Bài Viết'),
        'route' => 'manager.posts',
        'active' => $fn_active(['manager.posts']),
    ],
    [
        'title' => ('Quản Lý Chuyên Mục'),
        'icon' => 'fa fa-cubes',
        'route' => 'manager.categories',
        'active' => $fn_active(['manager.categories']),
    ],
    [
        'title' => ('Các Cài Đặt Khác'),
        'icon' => 'fa fa-cog',
        'route' => 'manager.present',
        'active' => $fn_active(['manager.present', 'manager.user']),
        'items' => [
            [
                'icon' => 'fa fa-cog',
                'title' => 'Giới Thiệu Sản Phẩm',
                'route' => 'manager.present',
                'active' => $fn_active(['manager.present'])
            ],
            [
                'icon' => 'fa fa-user',
                'title' => 'Tài Khoản',
                'route' => 'manager.user',
                'active' => $fn_active(['manager.user'])
            ],
        ]
    ],
    [
        'title' => ('Thoát'),
        'icon' => 'fa fa-sign-out',
        'route' => 'manager.logout',
        'active' => $fn_active(['manager.logout']),
    ]
];
?>

<div class="sidebar-wrapper ">
    <div class="toggle_menu">
        <i class="toggle_menu_icon">
            <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 30 30"
                 role="img" focusable="false">
                <path stroke="currentColor" stroke-linecap="round" stroke-miterlimit="10" d="M4 7h22M4 15h22M4 23h22"
                      stroke-width="3"></path>
            </svg>
        </i>
    </div>

    <h2 class="logo">
        <a href="{{ route('manager.posts') }}">
            <img src="/images/logo/logo_tp.png" alt="" style="width: 120px; height: 120px">
        </a>
    </h2>
    <ul class="main_menu sidebar-menu">
        @foreach($menus as $menu)
            @if (isset($menu['items']) && $menu['items'])
                <li class="has_child treeview {{ @$menu['active'] ? 'active show':'' }}" title="{{ $menu['title'] }}">
                    <a href="{{ route($menu['route']) }}">
                        <i class="{{ $menu['icon'] }}"></i>
                        <span>{{ $menu['title'] }}</span>
                    </a>
                    <ul class="treeview-menu">
                        @foreach($menu['items'] as $sub_menu)
                            <li class="{{ @$sub_menu['active'] ? 'active':'' }}" title="{{ $sub_menu['title'] }}">
                                <a href="{{ route($sub_menu['route']) }}">
                                    <i class="{{ $sub_menu['icon'] }}"></i>
                                    <span>{{ $sub_menu['title'] }}</span>
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </li>
            @else
                <li class="treeview {{ @$menu['active'] ? 'active show':'' }}" title="{{$menu['title']}}">
                    <a href="{{ route($menu['route']) }}"><i class="{{ $menu['icon'] }}"></i>
                        <span>{{ $menu['title'] }}</span></a>
                </li>
            @endif
        @endforeach
    </ul>
</div>
<!-- /#sidebar-wrapper -->
<script>
    function getScreenSize() {
        return $(window).width() > 992 ? 'md' : 'sm';
    }

    $(".sidebar-menu a").off("click").on("click", function (e) {
        var $li = $(e.currentTarget).closest('li');
        if (!$li.find('.treeview-menu').length || !$li.hasClass('treeview')) {
            return e;
        }

        if (getScreenSize() === 'md' && $('.page_body.menu_close').length) {
            return e;
        }

        if ($li.hasClass('show')) {
            $li.find('.treeview-menu').slideUp(function () {
                $li.removeClass('show').removeClass('active');
            });
            return false;
        }

        var $li_activing = $(".sidebar-menu .treeview.show").not($li);
        $li_activing.not(".has_child").removeClass('show').removeClass('active');
        $li_activing.find('.treeview-menu').slideUp(function () {
            $li_activing.removeClass('show').removeClass('active');
        });

        $li.find('.treeview-menu').slideDown(function () {
            $li.addClass('show').addClass('active');
        });
        return false;
    });

    $(".toggle_menu_icon").off("click").on("click", function (e) {
        $(".page_body").toggleClass('menu_close');
        $.cookie('toggle_menu_status_close', $(".page_body").hasClass('menu_close') ? 1 : 0, {path: '/'});
        return false;
    });
</script>
