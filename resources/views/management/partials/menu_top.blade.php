<?php
$user = getCurrentUser();
?>
<nav class="menu_top">
    <div class="page_title">@yield("heading_title")</div>
    <ul>
        <li>Xin Chào: <a href="{{ route('manager.user') }}">{{ $user->name }}</a></li>
        <li class="user_photo">
            <img src="{{ getPathImage($user->avatar) }}" alt="">
        </li>
    </ul>
</nav>
