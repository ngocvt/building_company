<div class="flash_message_list"><?php

if (isset($errors) && $errors->any()): ?>
<div class="alert alert-danger">
    @foreach ($errors->all() as $error)
        <div>{!! $error !!}</div>
    @endforeach
</div><?php
endif;

$message_types = ['primary', 'secondary', 'success', 'danger', 'warning', 'info', 'light', 'dark'];
foreach($message_types as $message_type):
$message = session('flash_' . $message_type);
if ($message): ?>
<div class="alert alert-{{ $message_type }}">
    {!! nl2br(e(trim($message))) !!}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div><?php
endif;
endforeach;
?></div>
