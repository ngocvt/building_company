@extends('application.layouts._app')
@section('body_class', 'main_page')
@section('page_title', 'Công Ty Xây Dựng Trường Phát')
@section('heading_title', 'Công Ty Xây Dựng Trường Phát')
@section('intro')
    @include('application.partials.intro')
@endsection
@section('main_menu')
    @include('application.partials.main_menu')
@endsection
@section('highlight_post')
    @include('application.partials.highlight_post', $highlightPosts)
@endsection
@section('category')
    @include('application.partials.category', $categories)
@endsection
@section('opinion_customer')
    @include('application.partials.opinion_customer')
@endsection
@section('footer')
    @include('application.partials.footer', $categories)
@endsection
