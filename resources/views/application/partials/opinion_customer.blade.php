<div class="wrapper bgded overlay" style="background-image:url('images/demo/backgrounds/03.png')">
    <section class="hoc container clear">
        <!-- ################################################################################################ -->
        <div class="sectiontitle center">
            <h6 class="heading">Venenatis quam aenean</h6>
            <p>Sodales blandit felis in <a href="#">venenatis facilisis</a></p>
        </div>
        <div class="group testimonials">
            <article class="one_half first"><img src="images/demo/100x100.png" alt="">
                <blockquote>Fusce dignissim ex ac fermentum volutpat cras euismod vitae odio non luctus cum sociis natoque penatibus et magnis dis parturient montes nascetur ridiculus mus in tempor.</blockquote>
                <h6 class="heading">John D.</h6>
                <em>Finibus enim vel</em></article>
            <article class="one_half"><img src="images/demo/100x100.png" alt="">
                <blockquote>Iaculis neque at euismod proin sed luctus ex etiam mattis tortor est at mattis enim ultrices quis donec sed accumsan arcu in varius magna aenean ut lectus semper lorem ut.</blockquote>
                <h6 class="heading">Jane D.</h6>
                <em>Pretium lectus sed</em></article>
        </div>
        <!-- ################################################################################################ -->
    </section>
</div>
