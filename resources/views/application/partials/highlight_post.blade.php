<section>
    <div class="center">
        <h6 class="heading">Sed justo sapien donec eget massa</h6>
        <p class="heading font-x3">Odio vestibulum id lacus vel lorem</p>
        <p>Ullamcorper dapibus curabitur imperdiet lacus et tincidunt tristique sapien ipsum aliquam nisl a vehicula
            nisl nisi in sem duis ut neque eu augue vulputate.</p>
    </div>
    <div class="hoc container section-grid">
        <!-- main body -->
        <!-- ################################################################################################ -->
        @foreach($highlightPosts as $key => $highlightPost)
            <div class="section-item">
                <a href="">
                    <img src="/images/demo/sky-wallpaper-anime_105141991.jpg" alt="" class="width-100">
                </a>
{{--                @if($key == 0)--}}
{{--                    <p>{{ $highlightPost->id }}</p>--}}
{{--                @endif--}}
                <a href="">{{$highlightPost->post_name}}</a>
                <p><code>{{ $highlightPost->description }}</code></p>
            </div>
        @endforeach

        {{--    <ul class="nospace group element btmspace-80">--}}
        {{--        @foreach($highlightPosts as $key => $highlightPost)--}}
        {{--        <li class="one_quarter @if($key === 0) first @endif">--}}
        {{--            <article>--}}
        {{--                <div class="txtwrap">--}}
        {{--                    <h6 class="heading">{{ $highlightPost->post_name }}</h6>--}}
        {{--                    <p>{{ $highlightPost->post_content }}</p>--}}
        {{--                    <footer><a href="#">Chi tiết</a></footer>--}}
        {{--                </div>--}}
        {{--            </article>--}}
        {{--        </li>--}}
        {{--        @endforeach--}}
        {{--    </ul>--}}
        {{--    <div class="row">--}}
        {{--      <div class="col-md-12">--}}
        {{--          <footer class="center"><a class="btn" href="#">Nisl sollicitudin porta</a></footer>--}}
        {{--          <!-- ################################################################################################ -->--}}
        {{--          <!-- / main body -->--}}
        {{--          <div class="clear"></div>--}}
        {{--      </div>--}}
        {{--    </div>--}}
    </div>
</section>
