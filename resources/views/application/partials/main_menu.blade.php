<?php
$companyName = 'Trường Phát';
$current_route = \Route::currentRouteName();
$fn_active = function ($routes = []) use ($current_route) {
    $routes = is_string($routes) ? [$routes] : $routes;
    foreach ($routes as $route) {
        if ($route == $current_route) {
            return true;
        }
    }

    return false;
};

$fn_is_active = function($menus) {
    foreach ($menus['items'] as $menu) {
        if ($menu['active']) {
            return true;
        }
    }

    return false;
};

$menus = [
    [
        'title' =>  ('Giới Thiệu'),
        'route' => '',
        'active' => $fn_active(['']),
    ],
    [
        'title' =>  ('Nhà Đẹp'),
        'route' => '',
        'active' => $fn_active(['']),
    ],
    [
        'title' =>  ('Kiến Trúc'),
        'route' => '',
        'active' => $fn_active(['']),
    ],
    [
        'title' =>  ('Nội Thất'),
        'route' => '',
        'active' => $fn_active(['']),
    ],
    [
        'title' =>  ('Kiến Thức Phong Thủy'),
        'route' => '',
        'active' => $fn_active(['']),
    ],
];
?>


<header id="header" class="container clear navbar-light">
    <div id="logo" class="fl_left">
        <h1>{{ $companyName }}</h1>
    </div>
    <nav id="mainav" class="fl_right">
        <ul class="clear">
            @foreach($menus as $menu)
            <li class="nav-item">
                <a class="nav-link" href="">{{ $menu['title'] }}</a>
            </li>
            @endforeach
        </ul>
    </nav>
</header>

<style>
    #header{
        padding: 0 0;
    }
    .nav-item {
        rgba(255,255,255,.7);
    }
</style>
