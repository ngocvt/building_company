<div class="wrapper row4" style="background: #95c5ed; color: black">
    <footer id="footer" class="hoc clear">
        <!-- ################################################################################################ -->
        <div class="center btmspace-50">
            <h2 class="heading">Halice</h2>
            <nav>
                <ul class="nospace inline pushright uppercase">
                    <li><a href=""><i class="fa fa-lg fa-home fa-fw"></i></a></li>
                @foreach($categories as $category)
                    <li><a href="#">{{ $category->categories_name }}</a></li>
                @endforeach
                </ul>
            </nav>
        </div>
        <!-- ################################################################################################ -->
        <hr class="btmspace-50">
        <!-- ################################################################################################ -->
        <div class="group">
            <div class="one_third first">
                <h6 class="heading">Augue curabitur vitae</h6>
                <ul class="nospace btmspace-30 linklist contact">
                    <li><i class="fa fa-map-marker"></i>
                        <address>
                            Street Name &amp; Number, Town, Postcode/Zip
                        </address>
                    </li>
                    <li><i class="fa fa-phone"></i> +00 (123) 456 7890</li>
                    <li><i class="fa fa-fax"></i> +00 (123) 456 7890</li>
                    <li><i class="fa fa-envelope-o"></i> info@domain.com</li>
                </ul>
            </div>
            <div class="one_third">
                <h6 class="heading">Velit volutpat lobortis</h6>
                <figure><a href="#"><img class="borderedbox inspace-10 btmspace-15" src="images/demo/320x168.png" alt=""></a>
                    <figcaption>
                        <h6 class="nospace font-x1"><a href="#">Lacus mattis rutrum eget</a></h6>
                        <time class="font-xs block btmspace-10" datetime="2045-04-06">Friday, 6<sup>th</sup> April 2045</time>
                    </figcaption>
                </figure>
            </div>
            <div class="one_third">
                <h6 class="heading">Magna vitae aliquet</h6>
                <p class="nospace btmspace-30">Tellus mauris volutpat luctus sapien ac luctus urna scelerisque nec.</p>
                <form method="post" action="#">
                    <fieldset>
                        <legend>Newsletter:</legend>
                        <input class="btmspace-15" type="text" value="" placeholder="Name">
                        <input class="btmspace-15" type="text" value="" placeholder="Email">
                        <button type="submit" value="submit">Submit</button>
                    </fieldset>
                </form>
            </div>
        </div>
        <!-- ################################################################################################ -->
    </footer>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<div class="wrapper" style="background: #0E76A8">
    <div id="copyright" class="hoc clear">
        <!-- ################################################################################################ -->
        <p class="fl_left">Copyright &copy; 2017 - All Rights Reserved - <a href="#">Domain Name</a></p>
        <p class="fl_right">Template by <a target="_blank" href="http://www.os-templates.com/" title="Free Website Templates">OS Templates</a></p>
        <!-- ################################################################################################ -->
    </div>
</div>
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<!-- ################################################################################################ -->
<a id="backtotop" href="#top"><i class="fa fa-home"></i></a>
