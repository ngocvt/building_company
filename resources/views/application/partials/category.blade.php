@foreach($categories as $category)
    <div class="wrapper bgded overlay" style="background-image:url('images/demo/nha-dep.jpg');">
        <article class="hoc cta clear">
            <!-- ################################################################################################ -->
            <div class="three_quarter first">
                <h6 class="header nospace">{{ $category->categories_name }}</h6>
                <p class="nospace">Iaculis morbi ultricies ullamcorper ipsum et blandit proin fringilla pharetra</p>
            </div>
            <footer class="one_quarter"><a class="btn" href="#">Tới chuyên mục {{ $category->categories_name }}</a>
            </footer>
            <!-- ################################################################################################ -->
        </article>
    </div>
    <div class="category_content hoc container" style="color: white;">
        <ul class="nospace group element btmspace-80">
            @foreach(getAllPostByCategoryId($category->id) as $key => $post)
                <li class="one_quarter @if($key === 0) first @endif">
                    <article>
                        <div class="txtwrap">
                            <h6 class="heading">{{ $post->post_name }}</h6>
                            <p>{{ $post->post_content }}</p>
                            <footer><a href="#">Chi tiết</a></footer>
                        </div>
                    </article>
                </li>
            @endforeach
        </ul>
    </div>
@endforeach
