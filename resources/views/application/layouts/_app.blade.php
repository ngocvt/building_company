<!doctype html>
<html lang="vie">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('page_title')</title>
    <script src="{{ mix('js/base.js') }}" type="text/javascript"></script>
    <script src="{{ url("/js/bluebird.min.js") }}" type="text/javascript"></script>
    <link rel="icon" href="{{ url('/icon.jpg') }}" type="image/x-icon">
    <link href="{{ mix('css/app.css') }}" rel="stylesheet" type="text/css">
</head>
<body id="top"  @yield('body_class')>
    <div class="bgded overlay" style="background-image:url('images/demo/backgrounds/01.png')">
        @yield('intro')
    </div>
    <div class="wrapper row1 bg-primary">
        @yield('main_menu')
    </div>
    <div class="wrapper row3">
        @yield('highlight_post')
    </div>
    <div class="category">
        @yield('category')
    </div>
    <div class="opinion_customer">
        @yield('opinion_customer')
    </div>
    <div class="footer">
        @yield('footer')
    </div>
</body>
</html>
<style>
    .category {
        background: white;
    }
</style>
