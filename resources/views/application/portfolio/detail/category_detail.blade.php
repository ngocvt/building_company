@extends('application.layouts.__app')
@section('page_title', $category->category_name)
@section('content')
    <div class="hero-image cover_detail_page">
        <div class="hero-text">
            <h1>{{ $category->category_name }}</h1>
        </div>
    </div>
    <style>
        .hero-image {
            background-image: linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url({{ asset($category->background_url) }});
            background-position: center;
            background-repeat: no-repeat;
            background-size: cover;
            position: relative;
            height: 400px;
        }

        .hero-text {
            text-align: center;
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
        }
        .hero-text h1 {
            color: white;
            font-size: 50px;
        }

        .post_content{
            margin: 20px auto;
            padding: 150px;
        }
        /*.image-style-side {*/
        /*    float: right;*/
        /*    max-width: 50%;*/
        /*}*/
    </style>
@endsection
