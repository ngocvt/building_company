<?php
$current_route = \Route::currentRouteName();
$fn_active = function ($routes = []) use ($current_route) {
    $routes = is_string($routes) ? [$routes] : $routes;
    foreach ($routes as $route) {
        if ($route == $current_route) {
            return true;
        }
    }

    return false;
};

$menus = [
    [
        'title' => ('Trang Chủ'),
        'route' => 'main_page',
        'active' => $fn_active(['main_page']),
    ],
    [
        'title' => ('Giới Thiệu'),
        'route' => 'main_page',
        'active' => $fn_active(['']),
    ],
    [
        'title' => ('Dự Án'),
        'route' => 'main_page',
        'active' => $fn_active(['']),
    ],
    [
        'title' => ('Videos'),
        'route' => 'main_page',
        'active' => $fn_active(['']),
    ],
];

$categoryMenu = [];
$categories = getAllCategory();
foreach ($categories as $category) {
    $categoryMenu['title'] = $category->category_name;
    $categoryMenu['route'] = 'category_detail';
    $categoryMenu['category_slug'] = $category->category_slug;
    $categoryMenu['active'] = '';
    array_push($menus, $categoryMenu);
}
array_push($menus, [
    'title' => ('Liên Hệ'),
    'route' => 'main_page',
    'active' => $fn_active(['']),
]);
?>

<aside id="colorlib-aside" role="complementary" class="js-fullheight">
    <h1 id="colorlib-logo"><a href="{{ route('main_page')}}">TRƯỜNG PHÁT</a></h1>
    <nav id="colorlib-main-menu" role="navigation">
        <ul>
            @foreach($menus as $menu)
                <li>
                    <a
                        href="{{ route($menu['route'], isset($menu['category_slug']) ? [$menu['category_slug']] : '')  }}"
                        class="{{ @$menu['active'] ? 'active':'' }} @if (isset($menu['category_slug']))
                        {{ request()->is('chuyen-muc/' . $menu['category_slug']) ? 'active' : '' }} @endif"
                    >
                        {{ $menu['title'] }}
                    </a>
                </li>
            @endforeach
        </ul>
        <div class="footer">
            <ul id="social_icon">
                <li><a href="#"><i class="icon-facebook2"></i></a></li>
                <li><a href="#"><i class="icon-twitter2"></i></a></li>
                <li><a href="#"><i class="icon-instagram"></i></a></li>
                <li><a href="#"><i class="icon-linkedin2"></i></a></li>
            </ul>
        </div>
        <div class="copy_icon">
            Copyright ©. 2020 All Rights Reserved.
        </div>
    </nav>
</aside>
<style>
    .footer {
        margin-top: 130px;
    }

    #social_icon {
        display: flex;
        justify-content: space-around;
        list-style-type: none;
    }

    .copy_icon {
        text-align: center;
        color: rgba(255, 255, 255, 0.5);
        font-size: 10px;
    }

    .active {
        color: #FFC300 !important;
    }

</style>
