<div class="colorlib-work">
    <div class="colorlib-narrow-content">
        <div class="row">
            <div class="col-md-6 col-md-offset-3 col-md-pull-3 animate-box" data-animate-effect="fadeInLeft">
                <span class="heading-meta">My Work</span>
                <h2 class="colorlib-heading animate-box">Recent Work</h2>
            </div>
        </div>
        <div class="row">
            @foreach($posts as $post)
                @if($post)
                    <div class="col-md-3 animate-box" data-animate-effect="fadeInLeft">
                        <!--Zoom effect-->
                        <div class="view overlay zoom">
                            <img src="{{ asset($post->post_image) }}" class="img-responsive " alt="smaple image">
                            <div class="mask flex-center">
                                <a href="{{ route('post_detail', [$post->post_slug]) }}">
                                    <p class="white-text" style="font-weight: bold">
                                        {{ $post->post_name }}
                                        <span style="font-style: italic; color: #BB898C; font-size: 12px">
                                            (Xem: {{ $post->views }})
                                        </span>
                                    </p>
                                </a>
                            </div>
                        </div>
                    </div>
                @endif
            @endforeach
        </div>
    </div>
</div>

{{--<div class="colorlib-blog">--}}
{{--    <div class="colorlib-narrow-content">--}}
{{--        <div class="row">--}}
{{--            <div class="col-md-6 col-md-offset-3 col-md-pull-3 animate-box" data-animate-effect="fadeInLeft">--}}
{{--                <span class="heading-meta">Read</span>--}}
{{--                <h2 class="colorlib-heading">Recent Blog</h2>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="row">--}}
{{--            <div class="col-md-4 col-sm-6 animate-box" data-animate-effect="fadeInLeft">--}}
{{--                <div class="blog-entry">--}}
{{--                    <a href="blog.html" class="blog-img"><img src=""--}}
{{--                                                              class="img-responsive" alt="">Posts</a>--}}
{{--                    <div class="desc">--}}
{{--                        <span>--}}
{{--                            <small></small>--}}
{{--                                <h3>--}}
{{--                                    <a href="">Posts</a>--}}
{{--                                </h3>--}}
{{--                            </span>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</div>--}}
