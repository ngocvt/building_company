<div class="colorlib-narrow-content">
    <div class="row">
        <div class="col-md-6 col-md-offset-3 col-md-pull-3 animate-box" data-animate-effect="fadeInLeft">
            <span class="heading-meta">What I do?</span>
            <h2 class="colorlib-heading">Here are some of my expertise</h2>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="row">
                <div class="col-md-12">
                    @foreach($presentTexts as $presentText)
                        @if($presentText)
                        <div class="colorlib-feature animate-box" data-animate-effect="fadeInLeft">
                        <div class="colorlib-icon">
                            <i class="{{ $presentText->icon }}"></i>
                        </div>
                        <div class="colorlib-text">
                            <h3>{{ $presentText->present_name }}</h3>
                            <p>{{ $presentText->description }}</p>
                        </div>
                    </div>
                        @endif
                    @endforeach
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="row">
                <div class="col-md-6">
                    @foreach($presentImages as $presentImage)
                        @if($presentImage)
                            <a href="" class="services-wrap animate-box" data-animate-effect="fadeInRight">
                        <div class="services-img"
                             style="background-image: url({{ getPathImage($presentImage->image_url)}});"></div>
                        <div class="desc">
                            <h3>{{ $presentImage->image_name }}</h3>
                        </div>
                    </a>
                        @endif
                    @endforeach
                </div>
                <div class="col-md-6 move-bottom">
                    @foreach($imgArrays as $imgArray)
                        @if($imgArray)
                            <a href="" class="services-wrap animate-box" data-animate-effect="fadeInRight">
                                <div class="services-img"
                                     style="background-image: url({{ getPathImage($imgArray->image_url)}});"></div>
                                <div class="desc">
                                    <h3>{{ $imgArray->image_name }}</h3>
                                </div>
                            </a>
                        @endif
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
<style>
    .services-wrap .desc {
        background: #99856d;
    }
</style>
