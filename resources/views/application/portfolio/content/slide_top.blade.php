<aside id="colorlib-hero" class="js-fullheight">
    <div class="flexslider js-fullheight">
        <ul class="slides">
            @foreach($posts as $key => $post)
                @if($post && getPublishCategoryById($post->category_id))
                    <li style="background-image: url({{ getPathImage(getPublishCategoryById($post->category_id)->background_url) }});">
                        <div class="overlay"></div>
                        <div class="container-fluid">
                            <div class="row">
                                <div
                                    class="col-md-6 col-md-offset-3 col-md-push-3 col-sm-12 col-xs-12 js-fullheight slider-text">
                                    <div class="slider-text-inner">
                                        <div class="desc">
                                            <h1>{{ getPublishCategoryById($post->category_id)->category_name }}</h1>
                                            <h2>{{ $post->post_name }}</h2>
                                            <h6>{{ $post->description }}</h6>
                                            <p>
                                                <a href="{{ route('post_detail', [$post->post_slug]) }}"
                                                   class="btn btn-primary btn-learn">
                                                    Xem Bài Viết
                                                    <i class="icon-arrow-right3"></i>
                                                </a>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                @endif
            @endforeach
        </ul>
    </div>
</aside>
