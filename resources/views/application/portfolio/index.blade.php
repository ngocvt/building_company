@extends('application.layouts.__app')
@section('page_title', 'Công Ty Xây Dựng Trường Phát')
@section('content')
    <div class="slide_top">
        @include('application.portfolio.content.slide_top')
    </div>
    <div id="intro" class="colorlib-about">
        @include('application.portfolio.content.intro')
    </div>
    <div class="colorlib-services">
        @include('application.portfolio.content.service')
    </div>

    <div id="colorlib-counter" class="colorlib-counters" style="background-image: url({{asset('frontend/images/cover_bg_1.jpg')}});" data-stellar-background-ratio="0.5">
        @include('application.portfolio.content.project')
    </div>
    <div class="detail_post">
        @include('application.portfolio.content.product')
    </div>
    <div class="contact">
        @include('application.portfolio.content.contact')
    </div>
@endsection
