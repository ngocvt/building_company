<?php
/**
 * Author: Xuan Hung
 * Date: 12-02-2019
 * Time: 10:44 AM
 * @var int $page
 * @var int $per_page
 * @var int $total_item
 * @var int $total_page
 * @var string $url
 */
$number_page_display = 5;
$start_page = max(1, $page - floor($number_page_display / 2));
$end_page = min($total_page, $start_page + $number_page_display - 1);
?>
<ul class="pagination justify-content-end">
    @if ($page == 1)
        <li class="page-item disabled">
            <a class="page-link" href="#" tabindex="-1" aria-disabled="true">Previous</a>
        </li>
    @else
        <li class="page-item">
            <a class="page-link" href="{{ str_replace('{i}', $page - 1, $url) }}">Previous</a>
        </li>
    @endif

    @for($page_index = $start_page; $page_index <= $end_page; $page_index++)
        @if ($page_index == $page)
            <li class="page-item active" aria-current="page">
                <a class="page-link" href="#">{{ $page_index }} <span class="sr-only">(current)</span></a>
            </li>
        @else
            <li class="page-item">
                <a class="page-link" href="{{ str_replace('{i}', $page_index, $url) }}">{{ $page_index }}</a>
            </li>
        @endif
    @endfor

    @if ($page == $total_page)
        <li class="page-item disabled">
            <a class="page-link" href="#" tabindex="-1" aria-disabled="true">Next</a>
        </li>
    @else
        <li class="page-item">
            <a class="page-link" href="{{ str_replace('{i}', $page + 1, $url) }}">Next</a>
        </li>
    @endif
</ul>
