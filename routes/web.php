<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::group([
    'namespace' => 'Application',
    'middleware' => 'web'
], function () {
    Route::get('/', 'ApplicationController@index')->name('main_page');
    Route::get('bai-viet/{post_slug}', 'ApplicationController@postDetail')->name('post_detail');
    Route::get('/chuyen-muc/{category_id}', 'ApplicationController@categoryDetail')->name('category_detail');
});

Route::group([
    'namespace' => 'Management',
    'prefix'    => 'manager'
], function () {
    Route::get('/', 'UserController@login')->name('manager.login');
    Route::post('/', 'UserController@login');
    Route::group([
        'middleware' => ['auth: manager']
    ], function () {
        Route::get('/logout', 'UserController@logout')->name('manager.logout');
        Route::get('/posts', 'PostController@index')->name('manager.posts');
        Route::post('/posts', 'PostController@getAllPost')->name('manager.post.list');
        Route::get('/post-management', 'PostController@createNewPost')->name('manager.create.post');
        Route::post('/post/register', 'PostController@savePost')->name('manager.save.post');
        Route::get('/post/edit', 'PostController@editPost')->name('manager.post.edit');
        Route::post('/delete', 'PostController@deletePost')->name('manager.post.delete');
        Route::get('/categories', 'CategoryController@index')->name('manager.categories');
        Route::post('/categories', 'CategoryController@getCategoryList')->name('manager.post.categories');
        Route::get('/category/', 'CategoryController@beautifulHouse')->name('manager.category.detail');
        Route::post('/category', 'CategoryController@beautifulHouseList')->name('manager.post.categories.detail');
        Route::post('upload', 'CkfinderController@uploadAction')->name('upload-image');
        Route::get('create/category', 'CategoryController@createCategory')->name('manager.create.category');
        Route::post('create/category', 'CategoryController@saveCategory')->name('manager.save.category');
        Route::get('/category/edit', 'CategoryController@editCategory')->name('manager.category.edit');
        Route::get('/present', 'PresentController@index')->name('manager.present');
        Route::post('/present', 'PresentController@getAll')->name('manager.present.list');
        Route::get('/create-present-text', 'PresentController@createText')->name('manager.creat.present.text');
        Route::get('/create-present-image', 'PresentController@createImage')->name('manager.creat.present.image');
        Route::post('/create-present-text', 'PresentController@saveText')->name('manager.save.present.text');
        Route::post('/create-present-image', 'PresentController@saveImage')->name('manager.save.present.image');
        Route::get('/present/edit', 'PresentController@editPresent')->name('manager.present.edit');
        Route::get('/user', 'UserController@index')->name('manager.user');
        Route::post('/user', 'UserController@saveData')->name('manager.save.user');
        Route::post('/publish', 'PostController@publishPost')->name('manager.publish.post');
    });
});
