const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
mix.options({
    processCssUrls: false
})
    .webpackConfig({
        devtool: 'source-map'
    })
;

mix.scripts([
    './node_modules/jquery/dist/jquery.js',
    './node_modules/bootstrap/dist/js/bootstrap.min.js',
    './node_modules/jquery.cookie/jquery.cookie.js',
    './node_modules/moment/min/moment.min.js',
    './node_modules/pc-bootstrap4-datetimepicker/build/js/bootstrap-datetimepicker.min.js',
    './resources/js/Utility/Http.js',
    './resources/js/Utility/HtmlHelper.js',
    './resources/js/Utility/BootstrapModal.js',
    './resources/js/Utility/ClientUtility.js',
    './resources/js/Utility/iEdit.js',
    './resources/js/app.js',
], 'public/js/base.js');

mix.scripts([
    './node_modules/@ckeditor/ckeditor5-build-classic/build/ckeditor.js',
], 'public/js/vendors.js');


mix.sass('resources/sass/app.scss', 'public/css')
    .sass('resources/sass/management.scss', 'public/css')
    .sourceMaps()
    .version()
;
