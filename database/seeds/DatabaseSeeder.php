<?php

use App\Models\Categories;
use App\Models\Company;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::insert([
            'name' => 'ngocvt',
            'email' => 'admin@admin.com',
            'password' => bcrypt('123456'),
            'is_management' => 1
        ]);

        Company::insert([
            'company_name' => 'truong-phat',
        ]);

        Categories::insert([
            [
                'category_name' => 'Nhà Đẹp',
                'category_slug' => 'nha-dep'
            ],
            [
                'category_name' => 'Kiến Trúc',
                'category_slug' => 'kien-truc',
            ],
            [
                'category_name' => 'Nội Thất',
                'category_slug' => 'noi-that'
            ],
            [
                'category_name' => 'Kiến Thức Phong Thủy',
                'category_slug' => 'kien-thuc-phong-thuy'
            ],
        ]);
    }
}
