<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->increments('post_id');
            $table->string('post_name',255);
            $table->string('post_slug',255);
            $table->text('description');
            $table->longText('post_content');
            $table->string('post_image');
            $table->integer('views')->default(0);
            $table->integer('post_highlight')->default(0);
            $table->integer('category_id')->default(1);
            $table->integer('delete_flag')->default(0);
            $table->integer('publish')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('posts');
    }
}
