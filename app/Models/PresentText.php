<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class PresentText
 * @package App\Models
 * @property string $present_name
 * @property string $description
 * @property string $icon
 * @property integer $publish
 */
class PresentText extends Model
{
    /**
     * @var string
     */
    protected $table = 'present_text';

    /**
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * @return PresentText[]|Collection
     */
    public function allPresentText()
    {
        return PresentText::all();
    }

    /**
     * @param $data
     * @param $id
     */
    public function saveData($data, $id)
    {
        $presentText = new PresentText();

        if ($id) {
            $presentText = $this->getPreSentTextById($id);
        }
        $presentText->present_name = $data['present_name'];
        $presentText->description = $data['description'];
        $presentText->icon = $data['icon'];
        $presentText->publish = $data['publish'];
        $presentText->save();
    }

    /**
     * @param $id
     * @return Builder|Model|object|null
     */
    public function getPreSentTextById($id)
    {
        return PresentText::query()
            ->where('id', $id)
            ->first();
    }

    /**
     * @return Builder[]|Collection
     */
    public function getPublishPresentText()
    {
        return PresentText::query()
            ->where('publish', '<>', 1)
            ->take(4)
            ->get();
    }
}
