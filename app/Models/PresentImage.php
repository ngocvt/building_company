<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class PresentImage
 * @package App\Models
 * @property string $image_name
 * @property string $image_url
 * @property int $publish
 */
class PresentImage extends Model
{
    /**
     * @var string
     */
    protected $table = 'present_image';

    /**
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * @return PresentImage[]|Collection
     */
    public function allPresentImage()
    {
        return PresentImage::all();
    }

    /**
     * @param $id
     * @return Builder|Model|object|null
     */
    public function getPresentImageById($id)
    {
        return PresentImage::query()
            ->where('id', $id)
            ->first();
    }

    /**
     * @param $data
     * @param $id
     */
    public function saveData($data, $id)
    {
        $presentImage = new PresentImage();
        if ($id) {
            $presentImage = $this->getPresentImageById($id);
        }

        $presentImage->image_name = $data['image_name'];
        $presentImage->image_url = $data['image_url'];
        $presentImage->publish = $data['publish'];
        $presentImage->save();
    }

    /**
     * @return Builder[]|Collection
     */
    public function getPublishPresentImage()
    {
        return PresentImage::query()
            ->where('publish', '<>', 1)
            ->take(5)
            ->get();
    }
}
