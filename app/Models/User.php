<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Hash;

/**
 * Class User
 * @package App
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property int $is_management
 * @property string avatar
 */
class User extends Authenticatable
{
    /**
     * @var string
     */
    protected $table = 'users';

    /**
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * @param string $userName
     * @return Builder|Model|object|null
     */
    public function getUserByName(string $userName) {
        return User::query()->where('name', '=', $userName)->first();
    }

    /**
     * @return bool
     */
    public function isManager() {
        return $this->is_management === 1 ? true : false;
    }

    public function saveData($data)
    {
        getCurrentUser()->name = $data['name'];
//        getCurrentUser()->password = Hash::make($data['password']);
        getCurrentUser()->avatar = $data['avatar'];
        getCurrentUser()->email = $data['email'];
        getCurrentUser()->is_management = 1;
        getCurrentUser()->save();
    }
}
