<?php


namespace App\Models;

use Illuminate\Console\Scheduling\Event;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Http\Request;

/**
 * Class Posts
 * @package App\Models
 * @property int $post_id
 * @property string $post_name
 * @property string $post_slug
 * @property string $description
 * @property string $post_content
 * @property string $post_image
 * @property int $post_highlight
 * @property int $views
 * @property int $category_id
 * @property int $publish
 * @property int $delete_flag
 */
class Posts extends Model
{
    /**
     * @var string
     */
    protected $table = 'posts';

    /**
     * @var string
     */
    protected $primaryKey = 'post_id';

    /**
     * @param $postData
     * @param $flag
     * @return Posts|Builder|Builder[]|Collection|Model
     */
    public function savePost($postData, $flag)
    {
        $post = new Posts();
        if ($flag) {
            $post = $this->getPost($flag);
        }
        $post->post_name = $postData['post_name'];
        $post->description = $postData['description'];
        $post->post_content = $postData['post_content'];
        $post->category_id = $postData['category_id'];
        $post->post_slug = $postData['post_slug'];
        $post->post_image = $postData['post_image'];
        $post->publish = $postData['publish'];
        $post->delete_flag = 0;
        $post->save();

        return $post;
    }

    public function publishPost($postId, $publish)
    {
        $post = Posts::query()
            ->where('post_id', $postId)
            ->findOrFail($postId);
        $post->publish = $publish;
        $post->save();
        return $post;
    }

    /**
     * @param $postId
     * @return Builder|Builder[]|Collection|Model
     */
    public function getPost($postId)
    {
        return Posts::query()
            ->where('post_id', $postId)
            ->findOrFail($postId);
    }

    /**
     * @param $postId
     * @param string $slug
     * @return Builder|Builder[]|Collection|Model
     */
    public function getPublishPost($postId, $slug)
    {
        $query = Posts::query()
            ->where('publish', '<>', 1);

        if ($postId) {
            $query->where('post_id', $postId);
        }

        if ($slug) {
            $query->where('post_slug', $slug);
        }

        return $query->first();
    }

    /**
     * @return HasOne
     */
    public function category()
    {
        return $this->hasOne(Categories::class, 'id', 'category_id');
    }

    /**
     * @param Request $request
     * @return Builder
     */
    public function getAll(Request $request)
    {
        $query = Posts::query()
            ->orderBy('post_id', 'DESC')
            ->where(function (Builder $query) use ($request) {
                $postName = $request->get('post_name');
                if (!$postName) {
                    return;
                }
                $query->where('post_name', 'LIKE', '%' . $postName . '%');
            });

        // Search by date
        if ($request->get('date_from')) {
            $query->whereDate('created_at', '>=', $request->get('date_from'));
        }

        if ($request->get('date_to')) {
            $query->whereDate('created_at', '<=', $request->get('date_to'));
        }

        $query->where('delete_flag', '<>', 1);

        return $query;
    }

    /**
     * @param Request $request
     * @return Builder
     */
    public function getAllPostByCategoryId(Request $request)
    {
        $categoryId = $request->get('category_id');
        $query = Posts::query()
            ->orderBy('created_at', 'DESC')
            ->where('category_id', $categoryId)
            ->where(function (Builder $query) use ($request) {
                $postName = $request->get('post_name');
                if (!$postName) {
                    return;
                }
                $query->where('post_name', 'LIKE', '%' . $postName . '%');
            });

        // Search by date
        if ($request->get('date_from')) {
            $query->whereDate('created_at', '>=', $request->get('date_from'));
        }

        if ($request->get('date_to')) {
            $query->whereDate('created_at', '<=', $request->get('date_to'));
        }

        $query->where('delete_flag', '<>', 1);
        $query->where('publish', '<>', 1);

        return $query;
    }

    /**
     * @param $id
     * @return Builder[]|Collection
     */
    public function getPostByCategoryId($id)
    {
        return Posts::query()
            ->orderBy('created_at', 'DESC')
            ->where('category_id', $id)
            ->where('delete_flag', '<>', 1)
            ->where('publish', '<>', 1)
            ->get();
    }

    /**
     * @param $postId
     * @return bool
     */
    public function deleteSoft($postId)
    {
        $post = Posts::query()->with('images')->where('post_id', $postId)->findOrFail($postId);
        $post->delete_flag = 1;
        return $post->save();
    }

    /**
     * @return Builder[]|Collection
     */
    public function getHighlightPost()
    {
        return Posts::query()
            ->orderBy('post_id', 'ASC')
            ->where('delete_flag', '<>', 1)
            ->take(5)
            ->get();
    }

    /**
     * @return array
     */
    public function getFirstPostOfCategory()
    {
        $posts = [];
        $categories = Categories::query()
            ->select('id')
            ->where('publish', '<>', 1)
            ->get();
        foreach ($categories as $category) {
            $post = Posts::query()
                ->where('category_id', $category->id)
                ->where('publish', '<>', 1)
                ->orderBy('created_at', 'DESC')
                ->first();
            array_push($posts, $post);
        }
        return $posts;
    }

    public function incrementView($slug)
    {
        return Posts::query()->where('post_slug', $slug)->increment('views');
    }
}
