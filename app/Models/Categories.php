<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

/**
 * Class Categories
 * @package App\Models
 * @property int $id
 * @property string $category_name
 * @property string $category_slug
 * @property int publish
 * @property string $background_url
 */
class Categories extends Model
{
    /**
     * @var string
     */
    protected $table = 'categories';

    /**
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * @param Request $request
     * @return Builder
     */
    public function getAllCategory(Request $request)
    {
        $query = Categories::query()
            ->where(function (Builder $query) use ($request) {
                $categoryName = $request->get('category_name');
                if (!$categoryName) {
                    return;
                }
                $query->where('category_name', 'LIKE', '%' . $categoryName . '%');
            });

        $query->orderBy('id', 'ASC');

        return $query;
    }

    /**
     * @return HasMany
     */
    public function posts() {
        return $this->hasMany(Posts::class, 'category_id', 'id');
    }

    /**
     * @param $id
     * @return Builder
     */
    public function getCategoryById(int $id)
    {
        return Categories::query()
            ->where('id', $id)
            ->first();
    }

    public function getCategoryBySlug($slug) {
        return Categories::query()
            ->where('category_slug', $slug)
            ->where('publish', '<>', 1)
            ->first();
    }

    /**
     * @param $name
     * @return Builder|Model|object|null
     */
    public function getCategoryByName(string $name)
    {
        return Categories::query()
            ->where('category_name', $name)
            ->first();
    }

    /**
     * @param $data
     * @param $flag
     */
    public function saveData($data, $flag)
    {
        $category = new Categories();

        if ($flag) {
            $category = $this->getCategoryById($flag);
        }

        $category->category_name = $data['category_name'];
        $category->category_slug = $data['category_slug'];
        $category->background_url = $data['background_url'];
        $category->publish = $data['publish'];
        $category->save();
    }
}
