<?php


namespace App\Http\Controllers\Management;


use App\Http\Controllers\Controller;
use App\Http\Request\Management\UserRequest;
use App\Models\User;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\View\View;

class UserController extends Controller
{
    const LOGGED_IN = "manager.posts";

    /**
     * @var User
     */
    private $user;

    /**
     * UserController constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * @param Request $request
     * @return Factory|RedirectResponse|View
     */
    public function login(Request $request)
    {
        if (isUserLoggedIn()) {
            return redirect()->route(self::LOGGED_IN);
        }

        if ($request->isMethod('post')) {
            if (!trim($request->get('user_name')) || !trim($request->get('password'))) {
                return redirect()->back()->withInput()->with('flash_danger', 'username or password empty !!!');
            }

            $user = $this->user->getUserByName($request->get('user_name'));
            if (!$user
                || !$user->isManager()
                || !Hash::check($request->get('password'), $user->password)
            ) {
                return redirect()->back()->withInput()->with('flash_danger', 'User name or Password invalid');
            }

            getAuthenticateGuard()->login($user);
            return redirect()->intended(route(self::LOGGED_IN));
        }
        return view('management.login.login');
    }

    /**
     * @return RedirectResponse
     */
    public function logout()
    {
        getAuthenticateGuard()->logout();
        return redirect()->route('manager.login');
    }

    public function index()
    {
        return view('management.user.index');
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     * @throws \Exception
     */
    public function saveData(Request $request)
    {
        $userName = $request->get('name');
//        $password = $request->get('password');
//        $oldPassword = $request->get('old_password');
        $avatar = $request->file('user_image');
        $email = $request->get('email');
//        if (!$userName || !$password) {
//            return redirect()->back()->withInput()->with('flash_danger', 'tên hoặc mật khẩu không được trống');
//        }
//
//        if (!Hash::check($oldPassword, getCurrentUser()->password)) {
//            return redirect()->back()->withInput()->with('flash_danger', 'Mật khẩu cũ không đúng');
//        }
//
//        if (Hash::check($password, getCurrentUser()->password)) {
//            return redirect()->back()->withInput()->with('flash_danger', 'Mật khẩu cũ và mới trùng nhau');
//        }
        if (!$avatar) {
            $fullPath = getCurrentUser()->avatar;
        } else {
            $size = [
                'width' => 150,
                'height' => 150,
            ];
            $fullPath = uploadImage($avatar, 'images/user/' . date('m-d-Y') . '/', $size);
        }

        $userData = [
            'name' => $userName,
            'avatar' => $fullPath,
//            'password' => $password,
            'email' => $email
        ];

        try {
            $this->user->saveData($userData);
            return redirect()->route('manager.posts')->with('flash_success', 'cập nhật thành công');
        } catch (\Exception $exception) {
            throw $exception;
        }
    }
}
