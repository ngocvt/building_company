<?php

namespace App\Http\Controllers\Management;

use App\Http\Controllers\Controller;
use App\Models\PresentImage;
use App\Models\PresentText;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\View\View;

/**
 * Class PresentController
 * @package App\Http\Controllers\Management
 */
class PresentController extends Controller
{
    /**
     * @var PresentImage
     */
    protected $presentImages;

    /**
     * @var PresentText
     */
    protected $presentTexts;

    /**
     * PresentController constructor.
     * @param PresentImage $presentImages
     * @param PresentText $presentTexts
     */
    public function __construct(PresentImage $presentImages, PresentText $presentTexts)
    {
        $this->presentImages = $presentImages;
        $this->presentTexts = $presentTexts;
    }

    /**
     * @return Factory|View
     */
    public function index()
    {
        return view('management.present.index');
    }

    /**
     * @return Factory|View
     */
    public function getAll()
    {
        $presentImages = $this->presentImages->allPresentImage();
        $presentTexts = $this->presentTexts->allPresentText();

        return view('management.present.present_list',
            compact('presentTexts', 'presentImages', 'presentImages'));
    }

    /**
     * @return Factory|View
     */
    public function createText()
    {
        $presentText = new PresentText();
        return view('management.present.new_present_text', compact('presentText'));
    }

    /**
     * @return Factory|View
     */
    public function createImage()
    {
        $presentImage = new PresentImage();
        return view('management.present.new_present_image', compact('presentImage'));
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     * @throws Exception
     */
    public function saveText(Request $request)
    {
        $name = $request->get('present_text_name');
        $description = $request->get('description');
        $icon = $request->get('icon');
        $id = $request->get('present_text_id');
        if (!$name) {
            return redirect()->back()->withInput()->with('flash_danger', 'Hãy nhập tên');
        }
        $textData = [
            'present_name' => $name,
            'description' => $description,
            'icon' => $icon,
            'publish' => $request->get('publish') ? 0 : 1,
        ];

        try {
            DB::beginTransaction();
            $this->presentTexts->saveData($textData, $id);
            DB::commit();
            $message = $request->get('present_text_id') ? 'Đã Cập Nhật' : 'Tạo Thành Công';
            Session::flash('flash_success', $message);

            return redirect()->route('manager.present')->with('flash_success', $message);
        } catch (Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     * @throws Exception
     */
    public function saveImage(Request $request)
    {
        $name = $request->get('present_image_name');
        $id = $request->get('present_image_id');
        $image = $request->file('image_present');
        if (!$name) {
            return redirect()->back()->withInput()->with('flash_danger', 'Hãy nhập tên');
        }

        if ($id && !$image) {
            $presentImage = $this->presentImages->getPresentImageById($id);
            $fullPath = $presentImage->image_url;
        } else {
            if (!$image) {
                return redirect()->back()->withInput()->with('flash_danger', 'Hãy thiết lập ảnh cho chuyên mục');
            }
            $size = [
                'width' => 300,
                'height' => 150,
            ];
            $fullPath = uploadImage($image, 'images/present/' . date('m-d-Y') . '/', $size);
        }

        $textData = [
            'image_name' => $name,
            'image_url' => $fullPath,
            'publish' => $request->get('publish') ? 0 : 1,
        ];

        try {
            DB::beginTransaction();
            $this->presentImages->saveData($textData, $id);
            DB::commit();
            $message = $request->get('present_image_id') ? 'Đã Cập Nhật' : 'Tạo Thành Công';
            Session::flash('flash_success', $message);

            return redirect()->route('manager.present')->with('flash_success', $message);
        } catch (Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
    }

    /**
     * @param Request $request
     * @return Factory|View|void
     */
    public function editPresent(Request $request)
    {
        $presentTextId = $request->get('present_text_id');
        $presentImageId = $request->get('present_image_id');

        if ($presentImageId) {
            $presentImage = $this->presentImages->getPresentImageById($presentImageId);
            return view('management.present.new_present_image', compact('presentImage'));
        }

        if ($presentTextId) {
            $presentText = $this->presentTexts->getPreSentTextById($presentTextId);
            return view('management.present.new_present_text', compact('presentText'));
        }

        return abort(404);
    }
}
