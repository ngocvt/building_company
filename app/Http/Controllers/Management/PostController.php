<?php

namespace App\Http\Controllers\Management;

use App\Http\Controllers\Controller;
use App\Models\Categories;
use App\Models\Posts;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\View\View;
use App\Pagination;

/**
 * Class PostController
 * @package App\Http\Controllers\Management
 */
class PostController extends Controller
{
    /**
     * @var Posts
     */
    private $post;

    /**
     * @var Categories
     */
    private $categories;


    /**
     * PostController constructor.
     * @param Posts $posts
     * @param Categories $categories
     */
    public function __construct(Posts $posts, Categories $categories)
    {
        $this->post = $posts;
        $this->categories = $categories;
    }

    /**
     * @return Factory|View
     */
    public function index()
    {
        return view('management.content.posts');
    }

    /**
     * @param Request $request
     * @return Factory|View
     */
    public function createNewPost(Request $request)
    {
        $categories = $this->categories->getAllCategory($request)->select('category_name', 'id')->get();
        $post = new Posts();
        return view('management.content.create_new_post', compact('categories', 'post'));
    }

    public function publishPost(Request $request) {
        $postId = $request->get('post_id');
        $publish = $request->get('publish_post');
        if(!$postId) {
            return response()->json([
                'message' => 'không tìm thấy bài viết',
            ]);
        }

        $post = $this->post->getPost($postId);
        if (!$post) {
            return response()->json([
                'message' => 'không tìm thấy bài viết',
            ]);
        }
        try {
            $response = $this->post->publishPost($postId, $publish);
            return response()->json([
                'message' => 'update thành công',
                'publish' => $response->publish
            ]);
        } catch (Exception $exception) {
            return response()->json([
                'message' => 'update không thành công',
            ]);
        }
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     * @throws Exception
     */
    public function savePost(Request $request)
    {
        $postTitle = $request->get('post_title');
        $postDescription = $request->get('post_description');
        $postContent = $request->get('post_content');
        $category = $request->get('category');
        $postImage = $request->file('image_post');
        $flag = $request->get('post_id');

        if (!$postTitle || !$postContent || !$category) {
            return redirect()->back()->withInput()->with('flash_danger', 'Hãy nhập đủ các mục cần thiết');
        }

        if ($flag && !$postImage) {
            $post = $this->post->getPost($flag);
            $fullPath = $post->post_image;
        } else {
            if (!$postImage) {
                return redirect()->back()->withInput()->with('flash_danger', 'Hãy nhập đủ các mục cần thiết');
            }
            $size = [
                'width' => 350,
                'height' => 150,
            ];
            $fullPath = uploadImage($postImage, 'images/avatar/' . date('m-d-Y') . '/', $size);
        }

        $postData = [
            'post_name' => $postTitle,
            'post_slug' => toSlug($postTitle),
            'description' => $postDescription,
            'post_content' => $postContent,
            'category_id' => $category,
            'post_image' => $fullPath,
            'publish' => $request->get('publish_post') ? 0 : 1,
        ];

        try {
            DB::beginTransaction();
            $this->post->savePost($postData, $flag);

            DB::commit();
            $message = $request->get('post_id') ? 'Đã Cập Nhật Bài Viết' : 'Tạo Bài Viết Thành Công';
            Session::flash('flash_success', $message);

            return redirect()->route('manager.posts')->with('flash_success', $message);
        } catch (Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
    }

    /**
     * @param Request $request
     * @return Factory|View
     */
    public function getAllPost(Request $request)
    {
        $query = $this->post->getAll($request);
        $paging = Pagination::create($query, $request->get('page'));
        $postList = $query->get();
        return view('management.content.post_list', compact('postList', 'paging'));
    }

    /**
     * @param Request $request
     * @return Factory|View
     */
    public function editPost(Request $request)
    {
        $post = $this->post->getPost($request->get('post_id'));
        $categories = $this->categories->getAllCategory($request)->select('category_name', 'id')->get();
        !isManagement() && !$post && abort(404);
        return view('management.content.create_new_post', compact('categories', 'post'));
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function deletePost(Request $request)
    {
        $postId = $request->get('post_id');
        if (!$postId) {
            return redirect()->route('manager.posts')->withInput()->with('flash_danger', 'Không tìm thấy bài viết này để xóa');
        }

        $this->post->deleteSoft($postId);

        return redirect()->route('manager.posts')->withInput()->with('flash_success', 'Đã xóa bài viết');
    }
}
