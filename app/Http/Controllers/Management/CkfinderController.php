<?php


namespace App\Http\Controllers\Management;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CkfinderController extends Controller
{
    /**
     * @param Request $request
     * @return array|void
     */
    public function uploadAction(Request $request)
    {
        $image = $request->file('upload');
        if(!$image) {
            return;
        }
        $path = 'images/posts/' . date('m-d-Y') .'/';
        $size = [
            'width' => 680,
            'height' => null,
        ];
        return [
            'url' => asset(uploadImage($image, $path, $size)),
            'uploaded' => true,
        ];
    }
}
