<?php


namespace App\Http\Controllers\Management;


use App\Http\Controllers\Controller;
use App\Http\Request\Management\CategoryRequest;
use App\Models\Categories;
use App\Models\Posts;
use App\Pagination;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;
use Illuminate\Support\Facades\Session;
use Exception;


/**
 * Class CategoryController
 * @package App\Http\Controllers\Management
 */
class CategoryController extends Controller
{
    /**
     * @var Categories
     */
    protected $categories;

    /**
     * @var Posts
     */
    protected $posts;

    /**
     * CategoryController constructor.
     * @param Categories $categories
     * @param Posts $posts
     */
    public function __construct(Categories $categories, Posts $posts)
    {
        $this->categories = $categories;
        $this->posts = $posts;
    }

    /**
     * @return Factory|View
     */
    public function index()
    {
        return view('management.categories.category');
    }

    /**
     * @param Request $request
     * @return Factory|View
     */
    public function getCategoryList(Request $request)
    {
        $query = $this->categories->getAllCategory($request);
        $paging = Pagination::create($query, $request->get('page'));
        $categories = $query->get();
        return view('management.categories.category_list', compact('categories', 'paging'));
    }

    /**
     * @param Request $request
     * @return Factory|View
     */
    public function beautifulHouse(Request $request)
    {
        $categoryId = $request->get('category_id');
        $category = $this->categories->getCategoryById($categoryId);
        return view('management.categories.beautiful_house', compact('category'));
    }

    /**
     * @param Request $request
     * @return Factory|View
     */
    public function beautifulHouseList(Request $request)
    {
        $query = $this->posts->getAllPostByCategoryId($request);
        $paging = Pagination::create($query, $request->get('page'));
        $postList = $query->get();
        return view('management.categories.beautiful_house_list', compact('postList', 'paging'));
    }

    public function createCategory()
    {
        $category = new Categories();
        return view('management.categories.create_category', compact('category'));
    }

    public function saveCategory(CategoryRequest $request)
    {
        $categoryName = $request->get('category_name');
        $backgroundUrl = $request->file('image_category');
        if (!$categoryName) {
            return redirect()->back()->withInput()->with('flash_danger', 'Hãy nhập tên chuyên mục');
        }

        $categoryId = $request->get('category_id');

        if (!$categoryId && $this->categories->getCategoryByName($categoryName)) {
            return redirect()->back()->withInput()->with('flash_danger', 'Chuyên Mục này đã tồn tại');
        }

        if ($categoryId && !$backgroundUrl) {
            $category = $this->categories->getCategoryById($categoryId);
            $fullPath = $category->background_url;
        } else {
            if (!$backgroundUrl) {
                return redirect()->back()->withInput()->with('flash_danger', 'Hãy thiết lập ảnh cho chuyên mục');
            }
            $size = [
                'width' => 600,
                'height' => 300,
            ];
            $fullPath = uploadImage($backgroundUrl, 'images/categories/' . date('m-d-Y') . '/', $size);
        }

        $categoryData = [
            'category_name' => $categoryName,
            'category_slug' => toSlug($categoryName),
            'background_url' => $fullPath,
            'publish' => $request->get('publish_category') ? 0 : 1,
        ];

        try {
            DB::beginTransaction();
            $this->categories->saveData($categoryData, $categoryId);
            DB::commit();
            $message = $request->get('category_id') ? 'Đã Cập Chuyên Mục' : 'Tạo Chuyên Mục Thành Công';
            Session::flash('flash_success', $message);

            return redirect()->route('manager.categories')->with('flash_success', $message);
        } catch (Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
    }

    public function editCategory(Request $request)
    {
        $category = $this->categories->getCategoryById($request->get('category_id'));
        !isManagement() && !$category && abort(404);
        return view('management.categories.create_category', compact('category'));
    }
}
