<?php


namespace App\Http\Controllers\Application;

use App\Http\Controllers\Controller;
use App\Models\Categories;
use App\Models\Company;
use App\Models\Posts;
use App\Models\PresentImage;
use App\Models\PresentText;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\View\View;

/**
 * Class ApplicationController
 * @package App\Http\Controllers\Application
 */
class ApplicationController extends Controller
{
    /**
     * @var Posts
     */
    protected $posts;

    /**
     * @var Categories
     */
    protected $categories;

    /**
     * @var PresentText
     */
    protected $presentTexts;

    /**
     * @var PresentImage
     */
    protected $presentImages;

    /**
     * ApplicationController constructor.
     * @param Posts $posts
     * @param Categories $categories
     * @param PresentText $presentTexts
     * @param PresentImage $presentImages
     */
    public function __construct(
        Posts $posts,
        Categories $categories,
        PresentText $presentTexts,
        PresentImage $presentImages)
    {
        $this->posts = $posts;
        $this->categories = $categories;
        $this->presentTexts = $presentTexts;
        $this->presentImages = $presentImages;
    }

    /**
     * @return Factory|View
     */
    public function index()
    {
        $posts = $this->posts->getFirstPostOfCategory();
        $presentTexts = $this->presentTexts->getPublishPresentText();
        $presentImages = $this->presentImages->getPublishPresentImage();
        if (!isset($_COOKIE['company'])) {
            setcookie('company', getCompanyName(), time() + 1 * 1 * 3600, '/');
            Company::increment('view');
        }
        $company = Company::query()->first();
        $imgArrays = [];
        foreach ($presentImages as $key => $presentImage) {
            if ($key % 2 !== 0) {
                $imgArrays[] = $presentImages[$key];
                unset($presentImages[$key]);
            }
        }
        return view('application.portfolio.index',
            compact('posts', 'presentTexts', 'presentImages', 'imgArrays', 'company'));
    }

    /**
     * @param $slug
     * @return Factory|View|void
     */
    public function postDetail($slug)
    {
        $post = $this->posts->getPublishPost($postId = 0, $slug);
        $category = $this->categories->getCategoryById($post->category_id);

        if (!$post || !$category) {
            abort(404);
        }

        if (!isset($_COOKIE['post_' . $post->post_id])) {
            setcookie('post_' . $post->post_id, $post->post_slug, time() + 1 * 1 * 3600, '/');
            $this->posts->incrementView($post->post_slug);
        }

        return view('application.portfolio.detail.post_detail', compact('category', 'post'));
    }

    /**
     * @param $categorySlug
     * @return Factory|View
     */
    public function categoryDetail($categorySlug)
    {
        $category = $this->categories->getCategoryBySlug($categorySlug);
        $posts = $this->posts->getPostByCategoryId($category->id);
        if (!$posts || !$category) {
            abort(404);
        }
        return view('application.portfolio.detail.category_detail', compact('category', 'posts'));
    }
}
