<?php


namespace App\Http\Request\Management;

use App\Http\Request\Request;
use Illuminate\Validation\Rule;

class UserRequest extends Request
{
    public function rules()
    {
        $userId = isManagement() ? $this->get('user_id') : getCurrentUser()->id;
        return [
            'name' => [
                'required',
                Rule::unique('users')
                    ->ignore($userId, 'id')
            ],
            'email' => [
                'email',
                'required',
                Rule::unique('users')
                    ->ignore($userId, 'id')
            ],
            'avatar' => [
                'image.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
            ],
//            'old_password' => 'required',
//            'password' => 'required|min:6|max:50|confirmed',
//            'password_confirmation' => 'required'
        ];
    }
}
