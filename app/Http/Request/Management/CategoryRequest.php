<?php


namespace App\Http\Request\Management;

use App\Http\Request\Request;
use Illuminate\Validation\Rule;

class CategoryRequest extends Request
{
    public function rules()
    {
        $categoryId = $this->get('category_id');
        return [
            'category_name' => [
                'required',
                Rule::unique('categories')
                    ->ignore($categoryId, 'id')
            ],
            'background_url' => [
                'image.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
            ]
        ];
    }

    public function messages()
    {
        return [
            'category_name.required' => 'Tên chuyên mục không được để trống',
            'background_url.required' => 'Ảnh chuyên mục không được để trống',
            'category_name.unique' => 'Tên chuyên mục đã tồn tại',
        ];
    }
}
