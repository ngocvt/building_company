<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Illuminate\Http\Request;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  Request  $request
     * @return string|null
     */
    protected function redirectTo($request)
    {
        if (isManagement()) {
            return route('manager.login');
        }
        return route('main_page');
    }

    public function handle($request, Closure $next, ...$guards)
    {
        $number_guards = is_array($guards) ? count($guards) : 0;
        $continue = false;
        $has_condition = false;
        $is_logged_in = getAuthenticateGuard()->check();
        $groups = [
            'manager'   => 'isManager',
        ];

        foreach ($guards as $guard_index => $guard) {
            $guard = trim($guard);

            if (!isset($groups[$guard])) {
                continue;
            }
            unset($guards[$guard_index]);
            $has_condition = true;
            $continue = $continue || $is_logged_in && getCurrentUser()->{$groups[$guard]}();
        }

        if ($has_condition && !$continue) {
            if (isManagement() || !$is_logged_in) {
                throw new AuthenticationException(
                    'Unauthenticated.', $guards, $this->redirectTo($request)
                );
            }
            abort(404);
        }

        // Check not condition:  not(group_name)
        $continue = $is_logged_in;
        foreach ($guards as $guard_index => $guard) {
            $guard = trim($guard);
            if (!preg_match("/not\(([^\)]+)\)/", $guard, $match)) {
                continue;
            }

            $user_group = $match[1];
            unset($guards[$guard_index]);

            if (!isset($groups[$user_group])) {
                continue;
            }

            $continue = $continue && !getCurrentUser()->{$groups[$user_group]}();
        }
        if ($is_logged_in && !$continue) {
            abort(404);
        }

        if (!$guards) {
            return $number_guards ? $next($request) : parent::handle($request, $next);
        }

        return call_user_func_array ('parent::handle', [$request, $next, array_values($guards)]);
    }
}
