<?php

use App\Models\Categories;
use App\Models\Posts;
use App\Models\User;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Intervention\Image\ImageManagerStatic;

if (function_exists('getCurrentUser')) {
    return;
}

function getCompanyName() {
    return 'truong-phat';
}

/**
 * @return bool
 */
function isUserLoggedIn()
{
    return getAuthenticateGuard()->check();
}

/**
 * @return User|Authenticatable|false
 */
function getCurrentUser()
{
    if (getAuthenticateGuard()->check()) {
        return getAuthenticateGuard()->user();
    }

    return false;
}

/**
 * @return Guard|Auth
 */
function getAuthenticateGuard()
{
    return Auth::guard(getAuthenticateGuardName());
}

/**
 * @return string
 */
function getAuthenticateGuardName()
{
    if (isManagement()) {
        return 'management';
    }
    return 'application';
}

/**
 * @return bool
 */
function isManagement()
{
    static $_isManagement;
    if (!is_bool($_isManagement)) {
        $_isManagement = strpos(Request::capture()->getPathInfo() . '/', '/manager/') === 0;
    }

    return $_isManagement;
}

/**
 * @return Builder[]|Collection
 */
function getAllCategory()
{
    return Categories::query()
        ->where('publish', '<>', 1)
        ->take(4)
        ->get();
}

/**
 * @param $id
 * @return Builder|Model|object|null
 */
function getPublishCategoryById($id)
{
    return Categories::query()
        ->where('id', $id)
        ->where('publish', '<>', 1)
        ->select('category_name', 'background_url')
        ->first();
}

/**
 * @param $id
 * @return Builder|Model|object|null
 */
function getCategoryById($id) {
    return Categories::query()
        ->where('id', $id)
        ->select('category_name', 'background_url')
        ->first();
}

/**
 * @param $categoryId
 * @return Builder[]|Collection
 */
function getAllPostByCategoryId($categoryId)
{
    return Posts::query()
        ->where('category_id', $categoryId)
        ->where('delete_flag', '<>', 1)
        ->get();
}

/**
 * @param $url
 * @return string
 */
function getPathImage($url)
{
    if (!$url) {
        return asset('images/app/no-image.png');
    }
    return asset($url);
}

/**
 * @param $str
 * @return string|string[]|null
 */
function toSlug($str)
{
    $str = trim(mb_strtolower($str));
    $str = preg_replace('/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/', 'a', $str);
    $str = preg_replace('/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/', 'e', $str);
    $str = preg_replace('/(ì|í|ị|ỉ|ĩ)/', 'i', $str);
    $str = preg_replace('/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/', 'o', $str);
    $str = preg_replace('/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/', 'u', $str);
    $str = preg_replace('/(ỳ|ý|ỵ|ỷ|ỹ)/', 'y', $str);
    $str = preg_replace('/(đ)/', 'd', $str);
    $str = preg_replace('/[^a-z0-9-\s]/', '', $str);
    $str = preg_replace('/([\s]+)/', '-', $str);
    return $str;
}

/**
 * @param $image
 * @param $destinationPath
 * @param $size
 * @return string
 */
function uploadImage($image, $destinationPath, $size)
{
    $imageType = ['jpg', 'jpeg', 'png', 'gift', 'bmp'];

    date_default_timezone_set('Asia/Ho_Chi_Minh');
    $fileName = date('m-d-Y_hisa') . '-' . $image->getClientOriginalName();

    if ($image && in_array(strtolower($image->getClientOriginalExtension()), $imageType)) {
        $link = $image->move($destinationPath, $fileName);
        ImageManagerStatic::make($link)
            ->encode("jpg", 100)
            ->fit($size['width'], $size['height'])
            ->save();
    }

    return $destinationPath . $fileName;
}
