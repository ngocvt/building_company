<?php
/**
 * Author: Xuan Hung
 * Date: 12-02-2019
 * Time: 10:16 AM
 */

namespace App;


use Illuminate\Contracts\View\Factory;
use Illuminate\Database\Query\Builder;
use Illuminate\View\View;

class Pagination
{
    const DEFAULT_PER_PAGE = 25;

    /**
     * @param Builder|\Illuminate\Database\Eloquent\Builder $query
     * @param int $page
     * @param int $per_page
     * @param string $url
     * @return Factory|View
     */
    public static function create($query, $page = 1, $per_page = self::DEFAULT_PER_PAGE, $url = '#{i}')
    {
        $total_item = $query->count();
        $per_page = is_numeric($per_page) && $per_page >= 1 ? $per_page : self::DEFAULT_PER_PAGE;
        $total_page = ceil($total_item / $per_page);
        $page = is_numeric($page) && $page > 1 ? $page : 1;
        $page = min($page, $total_page);

        $query->skip(($page - 1) * $per_page)->limit($per_page);

        return view('common.pagination', compact('page', 'per_page', 'total_item', 'total_page', 'url'));
    }
}
